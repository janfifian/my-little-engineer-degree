/**
 * 
 */
package data;

/**
 * A vector implementation of specimens. This one stores the specimen in 
 * the form of permutation vectors, i.e. vectors of numbers, 
 * in which all the numbers from 1 to n
 * are present and each one of these numbers appear exactly once.
 *
 * @author JanFifian
 */
public class VectorSpecimen implements Specimen {


	/**
	 * Generates a new specimen in a random way, using
	 * the provided random number generator.
	 * @param rng random number generator used in the process of
	 * construction for the new specimen. By default, it uses the 
	 * uniform distribution to ensure, that the resulting distribution
	 * of each specimen appearing is moreless the same.
	 */
	public VectorSpecimen(RandomNumberGenerator rng) {

	}

	/* (non-Javadoc)
	 * @see data.Specimen#crossover(data.Specimen)
	 */
	@Override
	public Specimen crossover(Specimen other) {
		// TODO Auto-generated method stub
		return null;
	}

	/* (non-Javadoc)
	 * @see data.Specimen#mutate()
	 */
	@Override
	public void mutate() {


	}

	/* (non-Javadoc)
	 * @see data.Specimen#evaluate(java.lang.String)
	 */
	@Override
	public void evaluate(String problemName) throws NameNotRecognizedException {
		// TODO Auto-generated method stub

	}

}
