package data;

/**
 * An interface for all possible representations of specimen.
 * @author JanFifian
 */
public interface Specimen {
	

	/**
	 * This method generates a new specimen by taking the given one and
	 * its provided partner and executing desired crossover method.
	 * 
	 * @param other -- the selected Specimen partner.
	 * @return another specimen, being an offspring to the selected order of parents.
	 */
	public Specimen crossover(Specimen other);
	
	/**
	 * This method performs a mutation on a given specimen, according to the mutation method provided.
	 * 
	 */
	public void mutate();
	

	/**
	 * A function which evaluates the specimen according to the given problem.
	 *  
	 * @param problemName a name of the problem, used to select the evaluation function.
	 * @throws NameNotRecognizedException in the case, where the provided argument
	 * fails to be a recognized problem name.
	 */
	public void evaluate(String problemName) throws NameNotRecognizedException;
}
