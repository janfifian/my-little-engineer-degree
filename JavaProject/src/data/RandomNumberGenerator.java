package data;

/**
 * A class providing source of randomness in the application.
 * It is used for generating brand new specimens to initialize the population,
 * as well as for providing a random influx in the mutation process. 
 * 
 * @author Jan Stulejarek
 */
public class RandomNumberGenerator {

	private int SingleSolutionSize;

	/**
	 * Constructs a new RNG basing on the distribution type provided.
	 * Currently, the only distributions which are implemented are:
	 * 
	 * --Poisson (argument: "Poisson")
	 * --Geometric (argument: "Geometric")
	 * 
	 * It is planned to expand that class beyond these limitations in
	 * the future. For generating new specimens, this class provides the
	 * methods which generate random numbers from the uniform range from 1 to
	 * some specified n.
	 * @param distributionName the name of the random distribution.
	 */
	public RandomNumberGenerator(String distributionName, int n) {
		this.SingleSolutionSize = n;
	}
}
