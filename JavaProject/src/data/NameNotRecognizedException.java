/**
 * 
 */
package data;

/**
 * Exception for the situations, where inappropriate name was provided for
 * some class requiring a string parameter from the given set.
 * @author Jan Stulejarek
 */
public class NameNotRecognizedException extends Exception {

	/**
	 * Generates a new exception for unrecognized name.
	 */
	public NameNotRecognizedException() {
	}

	/**
	 * Generates a new exception with some additional message.
	 * @param arg0
	 */
	public NameNotRecognizedException(String arg0) {
		super(arg0);
		// TODO Auto-generated constructor stub
	}

	/**
	 * Constructs an exception by wrapping a throwable object. 
	 * @param arg0
	 */
	public NameNotRecognizedException(Throwable arg0) {
		super(arg0);
		// TODO Auto-generated constructor stub
	}
}
