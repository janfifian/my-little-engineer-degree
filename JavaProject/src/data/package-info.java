/**
 * This package contains all the classes which hold the information about
 * the specimens as well as predefined containers for those objects.
 */

/**
 * @author JanFifian
 */
package data;