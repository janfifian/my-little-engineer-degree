#About this repository

This repository is meant to contain all my work connected to my Engineer Thesis and is supposed to contain three projects:

1. Unity version of game connected to genetic algorithms
2. Java or C++ framework for genetic algorithms and their applications in more theoretical problems.
3. A LaTeX document containing the thesis itself.

Throughout most of my work, this will be a private repository. This might change after my final exam.