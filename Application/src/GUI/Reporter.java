package GUI;

import Data.InputSpecification.InputParameters;
import Data.SpecimenSpecification.SpecimenRepresentation;
import Logic.GeneticAlgorithm.Evaluator;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;

/**
 * A class for generating reports from each launch of genethic algorithm.
 */
public class Reporter {
    /**
     * The filename (path) under which the reporter instance will try to save the report.
     */
    private String filename;

    /**
     * Constructs a new reporter instance to save the report from launch.
     * @param filename A path under which a report will be saved.
     */
    public Reporter(String filename){
        this.filename = filename;
    }

    /**
     * Creates a report from a single launch of genetic algorithm.
     * @param presets Description of the input parameters.
     * @param specimens Last generation of specimen.
     * @param evaluator The evaluator to evaluate each specimen from the last generation.
     * @throws IOException In the case when a file could not be saved under the appropriate filename
     * or in given location.
     */
    public void saveReport(String presets, ArrayList<SpecimenRepresentation> specimens, Evaluator evaluator) throws IOException {
        StringBuilder sb = new StringBuilder();
        sb.append("A report generated from the execution of Genetic Algorithm at: ");
        sb.append(java.util.Calendar.getInstance().getTime() );
        sb.append('\n');
        sb.append(presets);
        sb.append("\n Last generation specimens and their results:\n");
        for(int i = 0; i < specimens.size(); i++){
            sb.append(specimens.get(i).toString()+'\t'+evaluator.evaluateSpecimen(specimens.get(i)));
            sb.append('\n');
        }
        BufferedWriter writer = new BufferedWriter(new FileWriter(filename));
        writer.write(sb.toString());
        writer.close();
    }
}
