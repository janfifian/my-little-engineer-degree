package GUI;

import Data.Distributions.DistributionType;
import Data.Distributions.RandomDistribution;
import Data.InputSpecification.InputParameters;
import Data.InputSpecification.Pair;
import Data.ProblemSpecification.ProblemType;
import Data.SpecimenSpecification.SpecimenRepresentationType;
import Logic.GeneticAlgorithm.GeneticAlgorithm;
import Logic.GeneticAlgorithm.StopConditions.StopConditionType;
import Logic.Operators.CrossoverOperators.CrossoverOperationType;
import Logic.Operators.MutationOperators.MutationOperator;
import Logic.Operators.SelectionOperators.SelectionOperatorType;
import Logic.Operators.MutationOperators.MutationOperatorType;
import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;

public class MainGUIFrame {

    private static JFrame frame;
    private JPanel rootPane;
    private JTextPane outputGUIConsole;
    private JPanel inputPane;
    private JPanel outputPane;
    private JLabel launchOutputLabel;
    private JButton launchButton;
    private JProgressBar progressBarGA;
    private JComboBox<ProblemType> problemChoiceComboBox;
    private JComboBox permutationStoringTypeComboBox;
    private JLabel problemChoiceLabel;
    private JLabel programInputPanelLabel;
    private JLabel permutationStoringTypeLabel;
    private JComboBox crossoverOperatorSelectionComboBox;
    private JComboBox selectionOperatorSelectionComboBox;
    private JComboBox mutationOperatorSelectionComboBox;
    private JTextField mutationParameterInputTextField;
    private JLabel crossoverOperatorLabel;
    private JLabel selectionOperatorLabel;
    private JLabel mutationOperatorLabel;
    private JLabel mutationProbabilityDistributionLabel;
    private JLabel mutationProbabilityDistributionParameterLabel;
    private JButton selectInputCSVFileButton;
    private JSpinner populationSizeSpinner;
    private JComboBox mutationProbabilityDistributionSelectionComboBox;
    private JComboBox stopConditionComboBox;
    private JSpinner stopConditionParameterSpinner;
    private JLabel stopConditionLabel;
    private JLabel mutationProbabilityDistributionParameter2Label;
    private JTextField mutationParameter2InputTextField;
    private JLabel populationSizeLabel;
    private JLabel inputFileSelectionLabel;
    private JLabel inputFileSelectionStatusLabel;
    private JSeparator bottomSeparatingLine;
    private JSeparator topSeparatingLine;
    private JCheckBox influxCheckBox;
    private JSpinner influxPercentageSpinner;

    final JFileChooser fileChooser = new JFileChooser();

    private File inputCSVFile;
    private InputParameters params = new InputParameters();
    private GeneticAlgorithm GA;


    public void setExpectedIterationsLimit(int expectedIterationsLimit){
        progressBarGA.setMinimum(0);
        progressBarGA.setMaximum(expectedIterationsLimit);
    }

    public int getExpectedIterationsLimit(){
        return progressBarGA.getMaximum();
    }

    public void updateProgressBar(int currentIteration){
        if(currentIteration>progressBarGA.getMaximum()){
            progressBarGA.setMaximum(currentIteration+1);
        }
        progressBarGA.setValue(currentIteration);
    }

    /**
     * Constructs and launches the GUI frame instance.
     */
    public MainGUIFrame() {
        /**
         * Set spinner default values
         */

        populationSizeSpinner.setValue(Integer.valueOf(100));
        stopConditionParameterSpinner.setValue(Integer.valueOf(1000));
        /**
         * Sets Problem ComboBoxes correctly;
         */
        problemChoiceComboBox.setModel(new DefaultComboBoxModel(ProblemType.values()));
        permutationStoringTypeComboBox.setModel(new DefaultComboBoxModel(SpecimenRepresentationType.values()));
        crossoverOperatorSelectionComboBox.setModel(new DefaultComboBoxModel(CrossoverOperationType.values()));
        mutationOperatorSelectionComboBox.setModel(new DefaultComboBoxModel(MutationOperatorType.values()));
        selectionOperatorSelectionComboBox.setModel(new DefaultComboBoxModel(SelectionOperatorType.values()));
        mutationProbabilityDistributionSelectionComboBox.setModel(new DefaultComboBoxModel(DistributionType.values()));
        stopConditionComboBox.setModel(new DefaultComboBoxModel(StopConditionType.values()));
        /**
         * Selects input file to read and informs user, whether the file has been chosen properly;
         */

        inputFileSelectionStatusLabel.setForeground(Color.BLACK);
        selectInputCSVFileButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                int returnVal = fileChooser.showOpenDialog(frame);

                if (returnVal == JFileChooser.APPROVE_OPTION) {
                    inputCSVFile = fileChooser.getSelectedFile();
                    inputFileSelectionStatusLabel.setText("Chosen file: " + inputCSVFile.getName());
                    inputFileSelectionStatusLabel.setForeground(Color.BLUE);
                } else if(inputCSVFile!=null) {
                    inputFileSelectionStatusLabel.setText("Chosen file: " + inputCSVFile.getName());
                    inputFileSelectionStatusLabel.setForeground(Color.BLUE);
                } else {
                    inputFileSelectionStatusLabel.setText("Failed to choose a file.");
                    inputFileSelectionStatusLabel.setForeground(Color.RED);
                }
            }
        });

        /**
         * Launches the Genetic Algorithm Framework with given presets.
         */
        launchButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent)
            {
                new Thread(new Runnable() {
                    public void run() {
                        SwingUtilities.invokeLater(new Runnable() {
                            public void run() {
                                logClear();
                                log("Obtaining input parameters...\n");
                            }
                        });
                /*
                    Providing the graph file and problem type. This defines the evaluation function as well.
                 */
                        params.setEvaluator((ProblemType) problemChoiceComboBox.getSelectedItem(), inputCSVFile, MainGUIFrame.this);
                /*
                    Define the specimen representation.
                 */
                        params.setSpecimenRepresentation((SpecimenRepresentationType) permutationStoringTypeComboBox.getSelectedItem());
                /*
                    Setting influx and population size.
                 */
                        params.setInfluxSize((Integer) influxPercentageSpinner.getValue());
                        params.setPopulationSize((Integer) populationSizeSpinner.getValue());

                /*
                    Set genetic operators
                 */
                        params.setCrossoverOperator(((CrossoverOperationType) crossoverOperatorSelectionComboBox.getSelectedItem()).getOperator());
                        params.setSelectionOperator(((SelectionOperatorType) selectionOperatorSelectionComboBox.getSelectedItem()).getOperator());
                        params.setMutationOperator(((MutationOperatorType) mutationOperatorSelectionComboBox.getSelectedItem()).getOperator());
                /*
                    Check whether currently chosen genetic operators are compatible with selected representation type.
                 */
                        Pair<Boolean, Boolean> compatibility = params.determineCompatibility();
                        if (!compatibility.getFirst())
                            log("Incompatible mutation operator for the given specimen representation!!");
                        if (!compatibility.getSecond())
                            log("Incompatible crossover operator for the given specimen representation!!");
                /*
                    Setting distribution type for mutation operator
                 */
                        RandomDistribution distribution = DistributionType.getDistribution((DistributionType) mutationProbabilityDistributionSelectionComboBox.getSelectedItem(), mutationParameterInputTextField.getText(), mutationParameter2InputTextField.getText());
                        MutationOperator.setRandomDistribution(distribution);
                /*
                    Setting stop condition.
                 */
                        params.setStopCondition((StopConditionType) stopConditionComboBox.getSelectedItem(), stopConditionParameterSpinner.getValue().toString());
                /*
                    Launching the genetic algorithm with provided parameters.
                 */
                        GeneticAlgorithm GA = new GeneticAlgorithm(params);
                        GA.execute(MainGUIFrame.this);
                    }
                }
                ).start();
            }
        });
    }

    public void logClear(){
        outputGUIConsole.setText("");
    }

    public void log(String message){
        outputGUIConsole.setText( outputGUIConsole.getText()+'\n'+message);
    }

    public static void main(String[] args) {
        SwingUtilities.invokeLater(new Runnable() {
            public void run(){
        frame = new JFrame("QuAdruPedal");
        frame.setContentPane(new MainGUIFrame().rootPane);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
            }
        });
    }

    private InputParameters  getParameters(){
        InputParameters params = new InputParameters();

        return params;
    };
}
