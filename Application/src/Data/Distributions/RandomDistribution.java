package Data.Distributions;

import java.util.Random;

public abstract class RandomDistribution {

    protected Random RNG = new Random();
    /**
     * Generates a random integer according to the selected distribution
     * @return random integer from a given distribution
     */
    public abstract int generateMutationsNumber();

    /**
     * Generates a random integer from interval [a,b] (inclusive).
     * @param a start of the interval
     * @param b end of the interval
     * @return randomly generated integer from [a,b]
     */
    public int generateRandomNumberFromRange(int a, int b) {
        return (a + RNG.nextInt(b-a));
    }

    public Random getRandomEngine(){
        return RNG;
    }

    public void setRandomSeed(long seed){
        RNG.setSeed(seed);
    }
}
