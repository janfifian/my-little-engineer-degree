package Data.Distributions;

import java.util.Random;

/**
 * Represents a constant distribution, returning always 1.
 */
public class OnePointDistribution extends RandomDistribution {

    @Override
    public int generateMutationsNumber() {
        return 1;
    }
}
