package Data.Distributions;

public enum DistributionType {
    Uniform("Discrete Uniform Distribution (0,a)"),
    Binomial("Binomial Distribution (n,p)"),
    Constant("Constant One-Point Distribution (1,1)")
    ;

    private final String text;
    /**
     * @param txt
     */
    DistributionType(final String txt) {
        this.text = txt;
    }

    @Override
    public String toString() {
        return text;
    }

    /**
     * Converts DistributionType to actual distribution
     * @param type type of distribution to be obtained
     * @param param1 first parameter of the desired distribution
     * @param param2 second parameter of the desired distribution
     * @return a distribution of desired type with given parameters.
     */
    public static RandomDistribution getDistribution(DistributionType type, String param1, String param2){
        switch(type){
            case Uniform:
                return new UniformDistribution(Integer.parseInt(param1));
            case Binomial:
                return new BinomialDistribution(Integer.parseInt(param1),Float.parseFloat(param2));
            case Constant:
                return new OnePointDistribution();
            default:
                return null;
        }
    }
}
