package Data.Distributions;

import java.util.Random;

/**
 * A class representing a random distribution which is uniform on the set {0,1,2,...,n-1}.
 */
public class UniformDistribution extends RandomDistribution {

    /**
     * An upper bound on the interval.
     */
    private int upperBound;

    /**
     * Constructs a uniform distribution.
     * @param n the upper bound on the set.
     */
    UniformDistribution(int n){
        upperBound = n;
    }

    @Override
    public int generateMutationsNumber() {
        return RNG.nextInt(upperBound);
    }
}
