package Data.Distributions;

import java.util.ArrayList;
import java.util.Random;

import static java.lang.Math.pow;

/**
 * Provides the binomial random variable distribution.
 */

public class BinomialDistribution extends RandomDistribution {

    /**
     * Tabularized values od CDF for Bin(n,p) distribution.
     */
    private ArrayList<Double> cumulativeDistributionFunction;

    /**
     * Calculate binomial symbol "n over r"
     * @param n set power
     * @param r chosen elements
     * @return binomial symbol (n,r)
     */
    public static int binomialSymbol(int n, int r)
    {
        if (r > n / 2)
            r = n - r;

        int answer = 1;
        for (int i = 1; i <= r; i++) {
            answer *= (n - r + i);
            answer /= i;
        }

        return answer;
    }

    /**
     * Construct the Binomial distribution and precalculates the values of CDF for Bin(n,p).
     * @param n first parameter of this distribution, number of independent
     *          Bernoulli trials with success probability p.
     * @param p second parameter of this distribution, probability of success in a single
     *          Bernoulli trial.
     */
    public BinomialDistribution(int n, float p){
        cumulativeDistributionFunction = new ArrayList<>();
        cumulativeDistributionFunction.add(pow(1-p,n));
        for(int i = 1; i < n; i++){
            cumulativeDistributionFunction.add( binomialSymbol(n,i)*pow(p,i)*pow(1-p,n-i) );
        }
        cumulativeDistributionFunction.add(2.0);
    }

    @Override
    public int generateMutationsNumber() {
        double random = RNG.nextDouble();
        int ret=0;
        while(cumulativeDistributionFunction.get(ret) < random){
            ret++;
        }
        return ret;
    }
}
