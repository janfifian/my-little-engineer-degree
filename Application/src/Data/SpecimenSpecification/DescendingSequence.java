package Data.SpecimenSpecification;

import java.util.ArrayList;
import java.util.Random;

/**
 * Represents a permutation P as a n-element sequence A according to the following formula:
 * P(0) = A(0);
 * P(k) = A(k)-th element of {0,...,n-1} array after removing elements P(0),...,P(k-1);
 * In particular, (0,0,...,0) represents the identity permutation.
 */
public class DescendingSequence extends SpecimenRepresentation {
    /**
     * Random engine for generating new specimen
     */
    private static Random rand;
    /**
     * Permutation length
     */
    public static int problemSize;

    /**
     * Allows to access the array underneath the DescendingSequence object.
     * @return Sequence satisfying the condition A(k)<n-k-1 for all k<n
     */
    public ArrayList<Integer> getSequence() {
        return sequence;
    }

    /**
     * Sequence satisfying the condition A(k)<n-k for all k<n
     */
    private ArrayList<Integer> sequence;


    /**
     * Sets problem size and initializes random seed.
     * @param problemSize - length of permutation.
     */
    public static void setProblemSize(int problemSize){
        rand = new Random(System.currentTimeMillis());
        DescendingSequence.problemSize = problemSize;
    }

    /**
     * Generates a new random permutation, represented as DescendingSequence
     * @return random permutation as DescendingSequence
     */
    public static DescendingSequence generateRandomDescendingSequence(){
        ArrayList<Integer> A = new ArrayList<Integer>(problemSize);
        for(int i = 0; i < problemSize; i++){
            A.add(i,rand.nextInt(problemSize-i));
        }
        return new DescendingSequence(A);
    }

    /**
     * Constructs a DescendingSequence representation from a given integer array. This method assumes, that
     * argument is a proper representation for a DescendingSequence, i.e. A(i) < n-i for all i<n;
     * @param A - representation of a permutation as DescendingSequence
     */
    public DescendingSequence(ArrayList<Integer> A){
        this.sequence = A;
    }

    /**
     * Converts a Descending-sequence representation to an iniective integer array.
     * @return iniective integer array representing a permutation
     */
    public ArrayList<Integer> toArrayList(){
        ArrayList<Integer> source = new ArrayList<Integer>(problemSize);
        for(int i = 0; i < problemSize; i++){
            source.add(i,i);
        }
        ArrayList<Integer> result = new ArrayList<Integer>();
        for(int i = 0; i < problemSize; i++){
            int j = sequence.get(i);
            result.add(source.remove(j));
        }
        return result;
    }

    /**
     * Converts a DescendingSequence representation to IntegerVector one.
     * @return IntegerVector representing the same permutation as the DescendingSequence
     */
    public IntegerVector toIntegerVector(){
        return new IntegerVector(this.toArrayList());
    }

    @Override
    public String toString() {
        return this.toIntegerVector().toString();
    }
}
