package Data.SpecimenSpecification;

import Logic.GeneticAlgorithm.SpecimenComparator;

import java.util.ArrayList;
import java.util.Collections;

/**
 * A collection of specimens representing a single population.
 * @param <T> The proper class of Specimen Representation.
 */
public class Population<T extends SpecimenRepresentation> {

    /**
     * A comparator used to sort the specimen.
     */
    private static SpecimenComparator specimenComparator;

    /**
     * Array storing the population.
     */
    private ArrayList<T> specimens;

    /**
     * Defines the comparator used to sort the population.
     * @param sc Proper specimen comparator.
     */
    public static void setSpecimenComparator(SpecimenComparator sc){
        specimenComparator = sc;
    }

    /**
     * Constructs a new (empty) population instance.
     */
    public Population() {
        this.specimens = new ArrayList<T>();
    }

    /**
     * Constructs a new population instance from a given array of specimen.
     * @param specimens An array of specimen used to define a new population.
     */
    public Population(ArrayList<T> specimens){
        this.specimens = specimens;
    }

    /**
     * Copies specimens from another population, effectively replacing own specimens.
     * @param pop A population to replace currently held one.
     */
    public void setPopulation(Population pop){
        this.specimens = pop.getPopulation();
    }

    /**
     * Adds a specimen to the population.
     * @param specimen A proper representation of permutation to be added.
     */
    public void addSpecimen(T specimen){
        specimens.add( specimen);
    }

    /**
     * Combines the population with a given array of specimen.
     * @param specimen An array of specimen to be added into the population.
     */
    public void joinSpecimens(ArrayList<T> specimen){ specimens.addAll(specimen); }

    /**
     * Combines the population with another population.
     * @param pop A population to be blended into the current one.
     */
    public void joinSpecimens(Population<T> pop){ specimens.addAll(pop.getPopulation()); }

    /**
     * Returns the underlying array of specimen.
     * @return The population in the form of array.
     */
    public ArrayList<T> getPopulation(){ return specimens; }

    /**
     * Sorts the population according to the provided comparator.
     */
    public void sortPopulation(){
        Collections.sort(specimens,specimenComparator);
    }

    /**
     * Returns the comparator used to sort the population.
     * @return Comparator used for sorting the population.
     */
    public SpecimenComparator getComparator() {
        return specimenComparator;
    }
}
