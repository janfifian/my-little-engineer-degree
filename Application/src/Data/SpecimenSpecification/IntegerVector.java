package Data.SpecimenSpecification;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;

/**
 * A standard representation of n-element set permutation as a vector of n distinct integers
 */
public class IntegerVector extends SpecimenRepresentation {
    /**
     * A hash map instance representing the permutation in its explicit form.
     */
    private HashMap<Integer, Integer> Permutation;

    /**
     * Constructs an IntegerVector instance from a given hash map.
     * @param array A hash map representing the permutation.
     */
    public IntegerVector(HashMap<Integer, Integer>  array) {
        this.Permutation = array;
    }

    /**
     * Generates a random Integer Vector of a given size.
     * @param size Total length of the random permutation to be generated.
     * @return The new IntegerVector instance.
     */
    public static IntegerVector generateRandomIntegerVector(int size){
        ArrayList<Integer> array = new ArrayList<>();
        for(int i = 0; i < size; i++){
            array.add(i);
        }
        Collections.shuffle(array);
        return new IntegerVector(array);
    }

    /**
     * Constructs a new IntegerVector instance from a given array.
     * It is worth noticing, that this method does not check whether
     * the array is iniective, it is up to the user to provide a proper specimen.
     * @param array An iniective array based on which a new specimen will be constructed.
     */
    public IntegerVector(ArrayList<Integer> array){
        Permutation = new HashMap<Integer, Integer>();
        for(int i = 0; i < array.size(); i++){
            Permutation.put(i, array.get(i));
        }
    }

    @Override
    public String toString(){
        String result = "";
        for(int i = 0; i < Permutation.size(); i++){
            result+=Permutation.get(i)+ " ";
        }
        return result;
    }

    /**
     * Returns the size of this permutation.
     * @return Size of the permutation.
     */
    public int getSize(){
        return Permutation.size();
    }

    /**
     * Returns the element stored at n-th position, i.e. value of P(n).
     * @param n Argument to calculate value P(n).
     * @return Value of P(n).
     */
    public Integer getElementAt(int n){
        return Permutation.get(n);
    }

    /**
     * Sets the element at the given position in permutation.
     * This method does not guarantee, that after executing it the instance of object
     * will still represent a valid permutation.
     * @param pos Position on which the element is meant to be set.
     * @param element Element to be set.
     */
    public void setElementAt(int pos, int element){
        Permutation.put(pos,element);
    }
}
