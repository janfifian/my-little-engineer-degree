package Data.SpecimenSpecification;

/**
 * An abstract class for representing a specimen.
 */
public abstract class SpecimenRepresentation {
    /**
     * An attribute defining whether a specimen is supposed to be immune to mutation or not.
     * By default the specimen are prone to be subject of mutation.
     */
    private boolean mutability = true;

    /**
     * Sets the mutability parameter to a given value.
     * @param value Desired value.
     */
    public void setMutability(boolean value){
        mutability = value;
    }

    /**
     * Checks whether element is prone to mutation.
     * @return True if element can be subjected to mutation, false otherwise.
     */
    public boolean isMutable(){
        return mutability;
    }
}
