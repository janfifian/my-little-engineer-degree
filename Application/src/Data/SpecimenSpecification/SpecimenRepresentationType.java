package Data.SpecimenSpecification;

/**
 * An enum defining the type of permutation representation.
 */
public enum SpecimenRepresentationType  {
    INTEGER_VECTOR("Iniective Integer Vector"),
    DESCENDING_SEQUENCE("Descending Sequence of Numbers"),
    ORDINAL_NUMBER("Ordinal number")
        ;
    /**
     * Description of the permutation.
     */
    private final String text;

        /**
         * Creates a new instance of enum representing the method of storing
         * specimen in the memory.
         * @param text Prompt description of method.
         */
        SpecimenRepresentationType(final String text) {
        this.text = text;
        }

@Override
public String toString() {
        return text;
        }

        public static SpecimenRepresentationType defineType(final Class clas){
            if(clas==IntegerVector.class){
                return INTEGER_VECTOR;
            }
            if(clas==OrdinalNumber.class){
                return ORDINAL_NUMBER;
            }
            if(clas==DescendingSequence.class){
                return DESCENDING_SEQUENCE;
            }
            else{
                return null;
            }
        }
}
