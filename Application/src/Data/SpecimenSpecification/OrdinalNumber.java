package Data.SpecimenSpecification;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Random;

/**
 * A representation of permutation which can be interpreted as index of the permutation with respect
 * to the lexicographical ordering of all permutations of n-element set.
 */
public class OrdinalNumber extends SpecimenRepresentation {

    private static Random rand;
    /**
     * These two fields help us quicken the calculations by preserving the precalculated factorials from 1 to n.
     */
    private static int problemSize = 0;
    public static ArrayList<BigInteger> precalculatedFactorials;
    private BigInteger bigOrdinal;

    /**
     * Converts an lexicographical index number to standard permutation representation.
     * @return Permutation as an ArrayList
     */
    public ArrayList<Integer> toIntegerArrayList(){
        ArrayList<Integer> permutation = new ArrayList<Integer>(problemSize);
        ArrayList<Integer> source = new ArrayList<Integer>(problemSize);
        for(int i = 0; i < problemSize; i++){
            source.add(i,i);
        }

        BigInteger bi = new BigInteger(bigOrdinal.toString());
        for(int i = 0; i < problemSize; i++){
            BigInteger result[] = bi.divideAndRemainder(precalculatedFactorials.get(problemSize-i-1));
            int index = result[0].intValue();
            permutation.add(i,source.remove(index));
            bi = result[1];
        }
        return permutation;
    }

    /**
     * Converts OrdinalNumber representation to the IntegerVector one.
     * @return an IntegerVector instance equivalent to the prior ordinal number representation.
     */
    public IntegerVector toIntegerVector(){
        return new IntegerVector(this.toIntegerArrayList());
    }

    /**
     * Defines the size of every specimen and precalculates the values
     * of factorials up to problem size to ensure swiftness of calculations later on.
     * @param n desired problem size.
     */
    public static void setProblemSize(int n){
        problemSize = n;
        //Initializing random seed
        rand = new Random(System.currentTimeMillis());
        precalculatedFactorials = new ArrayList<BigInteger>(n+1);

        precalculatedFactorials.add( BigInteger.ONE);
        for(int i = 1; i <= n; i++){
            precalculatedFactorials.add(new BigInteger(String.valueOf(i)));
            precalculatedFactorials.set(i,precalculatedFactorials.get(i).multiply(precalculatedFactorials.get(i-1)));
        }

    }

    /**
     * Generates a random permutation as an ordinal number.
     * This method assumes, that problem size has been already specified.
     * @return the randomly generated permutation number
     */
    public static OrdinalNumber generateRandomOrdinalNumber() {
        BigInteger newOrdinal = new BigInteger(precalculatedFactorials.get(problemSize).bitLength(),rand);
        while(newOrdinal.compareTo(precalculatedFactorials.get(problemSize))>=0){
            newOrdinal= new BigInteger(precalculatedFactorials.get(problemSize).bitLength(),rand);
        }
        return new OrdinalNumber(newOrdinal);
    }

    /**
     * Gets the BigInteger representing the permutation
     * @return BigInteger representing the permutation
     */
    public BigInteger getBigOrdinal() {
        return bigOrdinal;
    }

    /**
     * Changes the underlying BigInteger representing the permutation
     * @param bigOrdinal - new ordinal representation.
     */
    public void setBigOrdinal(BigInteger bigOrdinal) {
        this.bigOrdinal = bigOrdinal;
    }

    /**
     * Constructs the OrdinalNumber representation of permutation.
     * @param bigOrdinal - lexicographical order index
     */
    public OrdinalNumber(BigInteger bigOrdinal){
        this.bigOrdinal = bigOrdinal;
    }

    @Override
    public String toString() {
        return this.toIntegerVector().toString();
    }

    /**
     * Returns the considered problem size.
     * @return problem size.
     */
    public static int getProblemSize(){
        return problemSize;
    }
}
