package Data.InputSpecification;

import Logic.GeneticAlgorithm.Evaluator;
import Data.ProblemSpecification.ProblemType;
import Data.SpecimenSpecification.Population;
import Logic.GeneticAlgorithm.SpecimenComparator;
import Data.SpecimenSpecification.SpecimenRepresentationType;
import GUI.MainGUIFrame;
import Logic.GeneticAlgorithm.StopConditions.*;
import Logic.Operators.CrossoverOperators.CrossoverOperator;
import Logic.Operators.MutationOperators.MutationOperator;
import Logic.Operators.SelectionOperators.SelectionOperator;

import java.io.File;
import java.io.IOException;

/**
 * Class keeping and interpreting the input from GUI and introducing it to an instance of genetic algorithm.
 */
public class InputParameters {
    /**
     * A StringBuilder instance which is supposed to store the information on
     * the provided parameters and display them on GUI console.
     */
    private StringBuilder presets = new StringBuilder();
    /**
     * A crossover operator instance defined by user via GUI.
     */
    private CrossoverOperator crossoverOperator;
    /**
     * A mutation operator instance defined by user via GUI.
     */
    private MutationOperator mutationOperator;
    /**
     * A selection operator instance defined by user via GUI.
     */
    private SelectionOperator selectionOperator;
    /**
     * Size of a population (each selection trims the population to this size).
     */
    private int populationSize;
    /**
     * A type of representation used to store permutations.
     */
    private SpecimenRepresentationType specimenRepresentationType;
    /**
     * An instance of evaluator used to calculate the fitness of each specimen according to the
     * graph describing particular problem considered..
     */
    private Evaluator eval;
    /**
     * Size of the influx defined by user via GUI.
     */
    private int influxSize;
    /**
     * A stop condition used to determine when does the algorithm ends.
     */
    private StopCondition stopCondition;

    /**
     * Returns the stop condition.
     * @return Stop Condition stored inside this InputParameters instance.
     */
    public StopCondition getStopCondition() {
        return stopCondition;
    }

    /**
     * Returns the preset values provided by user as a string.
     * @return String describing parameters of genetic algorithm provided by
     * user and stored InputParameters instance.
     */
    public String getPresetsAsString(){
        return presets.toString();
    }

    /**
     * Sets the stop condition to a given one.
     * @param stop an instance of stop condition to be stored.
     */
    public void setStopCondition(StopCondition stop){
        this.stopCondition = stop;
    }

    /**
     * Defines the stop condition to be stored.
     * @param stopType desired stop condition type.
     * @param param A string representing a parameter for a given stop condition type.
     */
    public void setStopCondition(StopConditionType stopType, String param) {
        presets.append("\nStop condition: ").append(stopType.toString());
        switch(stopType){
            case FunctionValueCondition:
                presets.append(" with expected specimen value below ").append(param);
                this.stopCondition = new EvaluationFunctionCondition(Double.parseDouble(param),false,100000,3600000);
                break;
            case TimeLimitCondition:
                long ttr = Long.parseLong(param);
                presets.append(" with ").append(ttr).append("miliseconds limit");
                this.stopCondition = new TimeCondition(ttr);
                break;
            case IterationLimitCondition:
                int itr = Integer.parseInt(param);
                presets.append(" with a limit of ").append(itr).append(" iterations");
                this.stopCondition = new IterationCondition(itr);
                break;
            default:
                break;
        }
    }

    /**
     * Returns the influx size defined by user via GUI.
     * @return an integer representing the influx size.
     */
    public int getInfluxSize() {
        return influxSize;
    }

    /**
     * Sets the influx size stored in the parameters.
     * @param influxSize influx size to be stored.
     */
    public void setInfluxSize(int influxSize)
    {
        if(influxSize>0){
            presets.append("\nInflux: ").append(influxSize).append(" random specimen");
        }
        else
        {
            presets.append("\nInflux: none");
        }
        this.influxSize = influxSize;
    }

    /**
     * Returns the defined crossover operator.
     * @return A crossover operator defined by user.
     */
    public CrossoverOperator getCrossoverOperator() {
        return crossoverOperator;
    }

    /**
     * Stores crossover operator in parameters instance.
     * @param crossoverOperator crossover operator to be stored.
     */
    public void setCrossoverOperator(CrossoverOperator crossoverOperator) {
        presets.append("\nCrossover operator: ").append(crossoverOperator.toString());
        this.crossoverOperator = crossoverOperator;
    }

    /**
     * Returns the defined mutation operator.
     * @return A mutation operator defined by user.
     */
    public MutationOperator getMutationOperator() {
        return mutationOperator;
    }

    /**
     * Stores mutation operator in parameters instance.
     * @param mutationOperator mutation operator to be stored.
     */
    public void setMutationOperator(MutationOperator mutationOperator) {
        presets.append("\nMutation operator: ").append(mutationOperator.toString());
        this.mutationOperator = mutationOperator;
    }

    /**
     * Returns the defined mutation operator.
     * @return A selection operator defined by user.
     */
    public SelectionOperator getSelectionOperator() {
        return selectionOperator;
    }

    /**
     * Stores selection operator in parameters instance.
     * @param selectionOperator selection operator to be stored.
     */
    public void setSelectionOperator(SelectionOperator selectionOperator) {
        presets.append("\nSelection operator: ").append(selectionOperator.toString());
        this.selectionOperator = selectionOperator;
        selectionOperator.setPopulationSize(this.populationSize);
    }

    /**
     * Sets the specimen representation to be used in genetic algorithm.
     * @param spec a specimen representation to be used.
     */
    public void setSpecimenRepresentation(SpecimenRepresentationType spec){
        presets.append("\nSpecimen representation chosen: ").append(spec.toString());
        this.specimenRepresentationType = spec;
    }

    /**
     * Returns the specimen representation type to be used in genetic algorithm.
     * @return the type of specimen representation stored inside the parameters instance.
     */
    public SpecimenRepresentationType getSpecimenRepresentation(){
        return this.specimenRepresentationType;
    }

    /**
     * Constructs and sets the evaluator according to the parameters provided via GUI.
     * @param problemType the type of problem to be solved.
     * @param filename the name of the file corresponding to the problem instance.
     * @param mainFrame a frame of GUI to provide the output console.
     */
    public void setEvaluator(ProblemType problemType, File filename, MainGUIFrame mainFrame){
        presets = new StringBuilder();
        presets.append("\nExecution performed for ").append(problemType.toString()).append(" problem described in file ").append(filename.getName());
        this.eval = new Evaluator(problemType);
        switch(problemType){
            case TSP:
                try {
                    eval.setDistanceGraph(filename, mainFrame);
                } catch (IOException e) {
                    e.printStackTrace();
                }
                break;
            case QAP:
                try{
                    eval.setDistanceAndCostGraph(filename,mainFrame);
                } catch (IOException e){
                    e.printStackTrace();
                }
                break;
            default:
                throw new IllegalStateException("Unexpected value of ProblemType: " + problemType);
        }
        Population.setSpecimenComparator(new SpecimenComparator(eval));
    }

    /**
     * Returns the defined evaluator.
     * @return Evaluator defined according to the GUI entries.
     */
    public Evaluator getEvaluator(){
        return this.eval;
    }

    /**
     * Returns the predefined population size.
     * @return Population size.
     */
    public int getPopulationSize() {
        return populationSize;
    }

    /**
     * Sets the population size to the desired value.
     * @param populationSize population size to be used in genetic algorithm.
     */
    public void setPopulationSize(int populationSize)
    {
        presets.append("\nPopulation Size: ").append(populationSize);
        this.populationSize = populationSize;
    }

    /**
     * Determines whether both mutation and crossover operator are compatible with
     * the specimen representation provided by user.
     * @return a pair of boolean variables, representing the compatibility of mutation operator (first slot)
     * and the compatibility of crossover operator with stored specimen representation type.
     */
    public Pair<Boolean, Boolean> determineCompatibility() {
        boolean mutationCompatibility = this.mutationOperator.isCompatible(this.specimenRepresentationType);
        boolean crossoverCompatibility = this.crossoverOperator.isCompatible(this.specimenRepresentationType);
        return new Pair<>(mutationCompatibility, crossoverCompatibility);
    }
}
