package Data.InputSpecification;

/**
 * A util template class which suits as an analogue of the C++ pair.
 * @param <A> type of first item.
 * @param <B> type of second item.
 */
public class Pair<A, B> {
    private A first;
    private B second;

    /**
     * Constructs an instance of pair from provided arguments.
     * @param first First item stored in this pair instance.
     * @param second Second  item stored in this pair instance.
     */
    public Pair(A first, B second) {
        super();
        this.first = first;
        this.second = second;
    }

    /**
     * Returns the hashcode of pair.
     * @return combined hashcode of items from pair.
     */
    public int hashCode() {
        int hashFirst = first != null ? first.hashCode() : 0;
        int hashSecond = second != null ? second.hashCode() : 0;

        return (hashFirst + hashSecond) * hashSecond + hashFirst;
    }

    /**
     * Tests whether two pairs are equal, provided that both first and second object has
     * properly defined 'equals' method.
     * @param other another object to be compared to the pair instance.
     * @return true if other object is an instance of pair for which both first
     * and second item equals the ones of the initial object.
     */
    public boolean equals(Object other) {
        if (other instanceof Pair) {
            Pair otherPair = (Pair) other;
            return
                    ((  this.first == otherPair.first ||
                            ( this.first != null && otherPair.first != null &&
                                    this.first.equals(otherPair.first))) &&
                            (  this.second == otherPair.second ||
                                    ( this.second != null && otherPair.second != null &&
                                            this.second.equals(otherPair.second))) );
        }

        return false;
    }

    /**
     * Converts a pair to a string according to the following formula
     * '(' + first +', ' + second +')'.
     * @return A string defined according to the formula above.
     */
    public String toString()
    {
        return "(" + first + ", " + second + ")";
    }

    /**
     * Returns the first argument of the pair instance.
     * @return first object stored in pair.
     */
    public A getFirst() {
        return first;
    }

    /**
     * Sets the first element of the pair.
     * @param first element to be stored on the first position of the pair.
     */
    public void setFirst(A first) {
        this.first = first;
    }


    /**
     * Returns the second argument of the pair instance.
     * @return second object stored in pair.
     */
    public B getSecond() {
        return second;
    }

    /**
     * Sets the second element of the pair.
     * @param second element to be stored on the second position of the pair.
     */
    public void setSecond(B second) {
        this.second = second;
    }
}