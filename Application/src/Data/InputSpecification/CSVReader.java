package Data.InputSpecification;

import Data.ProblemSpecification.DistanceGraph;
import Data.ProblemSpecification.FlowsGraph;
import GUI.MainGUIFrame;

import java.io.*;
import java.util.ArrayList;
import java.util.concurrent.Flow;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * A reader class used to interpret graphs saved as CSV files.
 */
public class CSVReader {
    /**
     * A character separating the numeric entries in CSV files.
     */
    private static final String FIELD_SEPARATOR = ";";

    /**
     * A function converting a line of numbers separated by character ';' to an array of string representing
     * particular numbers.
     */
    private Function<String, ArrayList<Float>> mapToItem = (line) -> mapToArray(line.split(FIELD_SEPARATOR));

    /**
     * A method converting an array of strings representing numbers into actual floats.
     * @param itemsToMap array of number-representing strings.
     * @return array of interpreted strings.
     */
    private ArrayList<Float> mapToArray(String[] itemsToMap){
        ArrayList<Float> row = new ArrayList<Float>();
        for (String s: itemsToMap) {
            row.add(Float.parseFloat(s));
        }
        return row;
    }

    /**
     * Creates pair of graphs according to specification of QAP problem
     * @param inputFile Path to input file
     * @param GUI Reference to GUI, which allows to communicate with the user
     * @return Pair of graphs, where first one is the distance graph and the second one is flows graph
     * @throws IOException In the case when Input error occurs
     */
    public Pair<DistanceGraph, FlowsGraph> readQAPGraph(File inputFile, MainGUIFrame GUI) throws IOException {

        BufferedReader br = new BufferedReader(new FileReader(inputFile));
        int n = Integer.parseInt(br.readLine());
        GUI.log("Reading Entries from file "+ inputFile.getName() + "..." );
        ArrayList<String> graphStringMatrix =new ArrayList<>();
        for(int i = 0; i < n; i++){
            graphStringMatrix.add(br.readLine() );
        }
        ArrayList<ArrayList<Float>> distances = new ArrayList<ArrayList<Float>>(graphStringMatrix.stream().map(mapToItem).collect(Collectors.toList()));
        DistanceGraph graph = new DistanceGraph();
        graph.setValues(distances);
        br.readLine();
        ArrayList<String> graphStringMatrix2 =new ArrayList<>();
        for(int i = 0; i < n; i++){
            graphStringMatrix2.add(br.readLine() );
        }
        br.close();
        ArrayList<ArrayList<Float>> flows = new ArrayList<ArrayList<Float>>(graphStringMatrix2.stream().map(mapToItem).collect(Collectors.toList()));
        FlowsGraph flowsGraph = new FlowsGraph();
        flowsGraph.setValues(flows);

        GUI.log("Successfully read problem graphs with "+ distances.size()  + " vertices." );
        return new Pair<DistanceGraph, FlowsGraph>(graph, flowsGraph);
    }


    /**
     * Creates a graph according to specification of QAP problem
     * @param inputFile Path to input file
     * @param GUI Reference to GUI, which allows to communicate with the user
     * @return Distance graph for TSP problem
     * @throws IOException In the case when Input error occurs
     */
    public DistanceGraph readGraph(File inputFile, MainGUIFrame GUI) throws IOException {

        BufferedReader br = new BufferedReader(new FileReader(inputFile));
        int n = Integer.parseInt(br.readLine());
        GUI.log("Reading Entries from file "+ inputFile.getName() + "..." );
        ArrayList<String> graphStringMatrix =new ArrayList<>();
        for(int i = 0; i < n; i++){
            graphStringMatrix.add(br.readLine() );
        }
        ArrayList<ArrayList<Float>> distances = new ArrayList<ArrayList<Float>>(graphStringMatrix.stream().map(mapToItem).collect(Collectors.toList()));
        br.close();
        DistanceGraph graph = new DistanceGraph();
        graph.setValues(distances);
        GUI.log("Successfully read problem graph with "+ distances.size()  + " vertices." );
        return graph;
    }
}
