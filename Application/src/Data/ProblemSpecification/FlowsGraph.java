package Data.ProblemSpecification;

import Data.SpecimenSpecification.IntegerVector;

import java.util.ArrayList;

/**
 * The graph class representing flows graph.
 */
public class FlowsGraph extends Graph {

    /**
     * Calculates total flow along the route defined via permutation.
     * Currently, this method has no particular use.
     * @param integerVector Route defined via permutation.
     * @return Total flow along the route.
     */
    private float calculateTotalCost(IntegerVector integerVector){
        float result = getEdge(integerVector.getSize()-1,0);
        for(int i=0; i < integerVector.getSize()-1; i++){
            result+=getEdge(i,i+1);
        }
        return result;
    }
}
