package Data.ProblemSpecification;

/**
 * An enum representing the type of problem to be solved.
 */
public enum ProblemType {
    QAP("Quadratic Assignment Problem"),
    TSP("Travelling Salesman Problem")
    ;

    private final String text;

    /**
     * Defines a problem type with a given text.
     * @param text
     */
    ProblemType(final String text) {
        this.text = text;
    }

    @Override
    public String toString() {
        return text;
    }
}