package Data.ProblemSpecification;

import java.util.ArrayList;

/**
 * A class representing a graph or a cost matrix.
 */
public class Graph {
    /**
     * We will use From-To notation, thus element with indices (a,b) denotes path from a to b
     */
    protected ArrayList<ArrayList<Float>> graph;

    /**
     * Defines the array of values defining the cost of each (i,j) edge.
     * @param values Array defining a graph.
     */
    public void setValues(ArrayList<ArrayList<Float>> values){
        this.graph = values;
    }

    /**
     * Returns a value of the edge (a,b) from the underlying graph instance.
     * @param a starting point of the edge to be returned.
     * @param b ending point of the edge to be returned.
     * @return the weight of (a,b) edge.
     */
    public float getEdge(int a, int b){
        return graph.get(a).get(b);
    }

    /**
     * Returns the size of the graph which defines the problem.
     * @return The size of the graph/problem.
     */
    public int getSize(){
        return graph.size();
    }
}
