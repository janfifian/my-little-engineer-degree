package Data.ProblemSpecification;

import Data.SpecimenSpecification.IntegerVector;

/**
 * A graph representing distances, allowing to calculate the total cost of TSP solution route.
 */
public class DistanceGraph extends Graph {

    /**
     * Calculates the total length (cost) of the TSP route represented via integer vector.
     * @param integerVector Integer Vector instance representing a permutation, which is to be evaluated.
     * @return Total cost of the route, aka the fitness value of the function.
     */
    public float calculateTotalDistance(IntegerVector integerVector){
        float result = getEdge(integerVector.getElementAt(integerVector.getSize()   -1),integerVector.getElementAt(0));
        for(int i=0; i < integerVector.getSize()-1; i++){
            result+=getEdge(integerVector.getElementAt(i),integerVector.getElementAt(i+1));
        }
        return result;
    }
}
