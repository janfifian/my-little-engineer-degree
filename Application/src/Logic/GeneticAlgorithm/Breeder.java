package Logic.GeneticAlgorithm;

import Data.SpecimenSpecification.Population;
import Data.SpecimenSpecification.SpecimenRepresentation;
import Logic.Operators.CrossoverOperators.CrossoverOperator;

import java.util.ArrayList;

public class Breeder<T extends SpecimenRepresentation> {
    private CrossoverOperator operator;

    public Breeder(CrossoverOperator operator) {
        this.operator = operator;
    }

    public Population totalCrossover(Population population){
        Population<T> nextPopulation = new Population<T>();
        ArrayList<T> recentPopulation = population.getPopulation();
        if(operator.isSymmetric()){
            for(int i = 0; i < recentPopulation.size(); i++){
                for(int j = i; j <recentPopulation.size();j++){
                    try {
                        symmetricCrossover(recentPopulation.get(i), recentPopulation.get(j), nextPopulation);
                    }
                    catch(Exception E){
                        E.printStackTrace();
                    }
                }
            }
        }
        else{
        for (T specimen1: recentPopulation){
            for (T specimen2: recentPopulation){
                try {
                    crossover(specimen1, specimen2, nextPopulation);
                    }
                catch (Exception E){
                        E.printStackTrace();
                    }
                }
            }
        }
        return nextPopulation;
    }

    private void symmetricCrossover(T A, T B, Population nextPopulation) throws Exception{
        nextPopulation.addSpecimen(operator.generateChild(A,B));
    }

    private void crossover(T A, T B, Population nextPopulation) throws Exception{
        //We do not need two copies of the same specimen;
        if(A.equals(B)){ nextPopulation.addSpecimen(A); return; }
        nextPopulation.addSpecimen(operator.generateChild(A,B));
        nextPopulation.addSpecimen(operator.generateChild(B,A));
    }
}
