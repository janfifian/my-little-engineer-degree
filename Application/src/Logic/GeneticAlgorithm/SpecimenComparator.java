package Logic.GeneticAlgorithm;

import Data.SpecimenSpecification.SpecimenRepresentation;
import Logic.GeneticAlgorithm.Evaluator;
import Logic.GeneticAlgorithm.StopConditions.EvaluationFunctionCondition;

import java.util.Comparator;

/**
 * An implementation of comparator which enables us to compare two specimens using provided instance of Evaluator.
 */
public class SpecimenComparator implements Comparator<SpecimenRepresentation> {
    private Evaluator evaluator;

    /**
     * Evaluates a given specimen using the provided comparator
     * @param specimen - a specimen to be evaluated
     * @return value of the cost/distance for the given specimen
     */
    public float calculateEvaluationFunction(SpecimenRepresentation specimen) {
        return evaluator.evaluateSpecimen(specimen);
    }

    /**
     * Changes the Evaluator instance to a provided one.
     * @param evaluator - new Evaluator instance, meant to be used when comparing specimen
     */
    public void setEvaluator(Evaluator evaluator){
        this.evaluator = evaluator;
    }

    /**
     * Constructs a new instance of SpecimenComparator with a given Evaluator.
     * @param eval - provided Evaluator instance, which is ready for use
     */
    public SpecimenComparator(Evaluator eval){
        this.evaluator = eval;
    }

    /**
     * Compares two specimen with respect to a given problem specification
     * @param t1 - first specimen to compare
     * @param t2 - second specimen to compare
     * @return - 1 if specimen t1 has a higher cost/distance than specimen t2;
     *
     * 0 if both specimen have the same cost/distance;
     *
     * -1 otherwise;
     */
    @Override
    public int compare(SpecimenRepresentation t1, SpecimenRepresentation t2)
    {
        Float ev1 = evaluator.evaluateSpecimen(t1);
        Float ev2 = evaluator.evaluateSpecimen(t2);
        return (ev1.compareTo(ev2));
    }

    /**
     * Returns the instance of evaluator used to perform all the calculations.
     * @return Evaluator used to calculate fitness.
     */
    public Evaluator getEvaluator(){
        return evaluator;
    }
}
