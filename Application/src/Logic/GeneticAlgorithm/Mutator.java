package Logic.GeneticAlgorithm;

import Data.SpecimenSpecification.Population;
import Data.SpecimenSpecification.SpecimenRepresentation;
import Logic.Operators.MutationOperators.MutationOperator;

import java.util.ArrayList;

public class Mutator<T extends SpecimenRepresentation> {
    private MutationOperator operator;
    public Mutator(MutationOperator mutationOperator) {
    this.operator = mutationOperator;
    }

    public void mutatePopulation(Population population){
        ArrayList<T> pop = population.getPopulation();
        for (SpecimenRepresentation specimen: pop) {
            if(specimen.isMutable()) {
                operator.mutate(specimen);
            }
            else{
                specimen.setMutability(true);
            }
        }
    }
}
