package Logic.GeneticAlgorithm.StopConditions;

public class IterationCondition implements StopCondition{
    private int iterationsToBeDone;

    public IterationCondition(int iterations){
        this.iterationsToBeDone = iterations;
    }

    public int getIterationsToBeDone() {
        return iterationsToBeDone;
    }

    @Override
    public boolean checkWhetherSatisfied(int iterationsMade, double bestSpecimenEvaluationFunctionValue, long timePassed) {
        return (iterationsMade>=iterationsToBeDone);
    }
}
