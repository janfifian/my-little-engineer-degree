package Logic.GeneticAlgorithm.StopConditions;

public class TimeCondition implements StopCondition {
    /**
     * Time in milliseconds
     */
    private long timeRemaining;

    public TimeCondition(long timeToRun){
        /**
         * Time in milliseconds.
         */
        this.timeRemaining = timeToRun;
    }

    public long getTimeRemaining(){
        return timeRemaining;
    }

    public double getTimeLimit(){
        return timeRemaining;
    }
    @Override
    public boolean checkWhetherSatisfied(int iterationsMade, double bestSpecimenEvaluationFunctionValue, long timePassed) {
        return timePassed>=timeRemaining;
    }
}
