package Logic.GeneticAlgorithm.StopConditions;

public class EvaluationFunctionCondition implements StopCondition{
    private double expectedValue;
    private boolean isMaximizationProblem;
    private long maxIterations;
    private double maxTimeValue;

    public EvaluationFunctionCondition(double expectedValueToReach, boolean isItMaximizationProblem, long hardcodedIterationsLimit, double hardcodedTimeLimit){
        this.expectedValue = expectedValueToReach;
        this.isMaximizationProblem = isItMaximizationProblem;
        this.maxIterations = hardcodedIterationsLimit;
        this.maxTimeValue = hardcodedTimeLimit;
    }

    @Override
    public boolean checkWhetherSatisfied(int iterationsMade, double bestSpecimenEvaluationFunctionValue, long timePassed) throws UnableToSatisfyException {
        if(iterationsMade>maxIterations){
            throw new UnableToSatisfyException();
        }
        if(timePassed>maxTimeValue){
            throw new UnableToSatisfyException(timePassed);
        }

        return isMaximizationProblem ? (bestSpecimenEvaluationFunctionValue>=expectedValue) : (bestSpecimenEvaluationFunctionValue <= expectedValue);
    }

    public class UnableToSatisfyException extends Exception {
        UnableToSatisfyException(){
            super("The program was unable to find the desired solution in the hardcoded limit of iterations which equals " + maxIterations +".\n");
        }
        UnableToSatisfyException(double timePassed){
            super("The program was unable to find the desired solution during last " + timePassed + " seconds due to the hardcoded limit of time which equals " + maxTimeValue +" seconds.\n");
        }
    }
}
