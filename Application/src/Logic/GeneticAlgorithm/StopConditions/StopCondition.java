package Logic.GeneticAlgorithm.StopConditions;

public interface StopCondition {
    /**
     * Returns true in the case when program should be stopped.
     * @param iterationsMade
     * @param bestSpecimenEvaluationFunctionValue
     * @param timePassed
     * @return
     */
    public boolean checkWhetherSatisfied(int iterationsMade, double bestSpecimenEvaluationFunctionValue, long timePassed) throws EvaluationFunctionCondition.UnableToSatisfyException;
}
