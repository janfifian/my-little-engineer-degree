package Logic.GeneticAlgorithm.StopConditions;

import Logic.Operators.CrossoverOperators.CompositionCrossover;
import Logic.Operators.CrossoverOperators.MaximalPreservationCrossover;

public enum StopConditionType {
    TimeLimitCondition("Time limit (milliseconds):"),
    IterationLimitCondition("Iteration limit (iterations):"),
    FunctionValueCondition("Desired function value:")
    ;
    private String text;
    /**
     * @param text
     */
    StopConditionType(final String text) {
        this.text = text;
    }

    /* (non-Javadoc)
     * @see java.lang.Enum#toString()
     */
    @Override
    public String toString() {
        return text;
    }
}