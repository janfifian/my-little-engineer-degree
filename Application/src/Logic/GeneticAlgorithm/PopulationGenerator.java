package Logic.GeneticAlgorithm;

import Data.SpecimenSpecification.*;

/**
 * This class serves the general purpose of generating both influx and initial population
 * @author Jan Fifian
 */
public class PopulationGenerator {
    /**
     * Specifies the size of problem, thus the size of specimens.
     */
    private int problemSize;
    /**
     * The type of representation we are using.
     */
    private SpecimenRepresentationType type;

    /**
     * Constructs a new generator by introducing the problem size and the type of representation.
     * @param type Type of specimen representation
     * @param problemSize Size of the problem
     */
    public PopulationGenerator(SpecimenRepresentationType type, int problemSize){
        this.type = type;
        this.problemSize = problemSize;
    }

    /**
     * Generates a new population of a given size.
     * @param populationSize The size of the initialized population
     * @return A new population of a desired size, generated randomly.
     */
    public Population generateInitialPopulation(int populationSize){
        switch(type){
            case INTEGER_VECTOR:
                Population<IntegerVector> P1 = new Population<>();
                for (int i = 0; i < populationSize ; i++) {
                    IntegerVector V = IntegerVector.generateRandomIntegerVector(problemSize);
                    P1.addSpecimen(V);
                }
                return P1;
            case ORDINAL_NUMBER:
                Population<OrdinalNumber> P2 = new Population<>();
                OrdinalNumber.setProblemSize(problemSize);
                for (int i = 0; i < populationSize ; i++) {
                    P2.addSpecimen(OrdinalNumber.generateRandomOrdinalNumber());
                }
                return P2;
            case DESCENDING_SEQUENCE:
                Population<DescendingSequence> P3 = new Population<>();
                DescendingSequence.setProblemSize(problemSize);
                for (int i = 0; i < populationSize ; i++) {
                    P3.addSpecimen(DescendingSequence.generateRandomDescendingSequence());
                }
                return P3;
        }
        return null;
    }

    /**
     * A method introducing the new specimen to the existing population.
     * @param oldPopulation Population which is to be extended via introducing new specimen.
     * @param influxSize The number of new specimens to be introduced.
     * @param <T> Specimen representation class
     */
    public <T extends SpecimenRepresentation> void generateInflux(Population<T> oldPopulation ,int influxSize){
        Population<T> pop = generateInitialPopulation(influxSize);
        oldPopulation.joinSpecimens(pop);
    }
}
