package Logic.GeneticAlgorithm;

import Data.InputSpecification.CSVReader;
import Data.InputSpecification.Pair;
import Data.ProblemSpecification.DistanceGraph;
import Data.ProblemSpecification.FlowsGraph;
import Data.ProblemSpecification.ProblemType;
import Data.SpecimenSpecification.*;
import GUI.MainGUIFrame;

import java.io.File;
import java.io.IOException;


/**
    Class enabling the user to calculate value of the minimized function for a given specimen.
 */
public class Evaluator {
    private ProblemType problemType;
    private DistanceGraph distanceGraph;
    private FlowsGraph flowsGraph;
    private int N;

    public Evaluator(FlowsGraph flowsGraph, DistanceGraph distanceGraph) {
        this.flowsGraph = flowsGraph;
        this.distanceGraph = distanceGraph;
        problemType = ProblemType.QAP;
        N = flowsGraph.getSize();
    }

    public int getProblemSize(){
        return N;
    }

    public Evaluator(ProblemType problemType){
        this.problemType = problemType;
    }

    public void setDistanceAndCostGraph(File file, MainGUIFrame guiFrame) throws IOException {
        CSVReader reader = new CSVReader();
        Pair<DistanceGraph, FlowsGraph> twinGraphs= reader.readQAPGraph(file, guiFrame);
        this.flowsGraph = twinGraphs.getSecond();
        this.distanceGraph = twinGraphs.getFirst();
        N = flowsGraph.getSize();
    }

    public void setDistanceGraph(File file, MainGUIFrame guiFrame) throws IOException {
        CSVReader reader = new CSVReader();
        this.distanceGraph = reader.readGraph(file, guiFrame);
        N = distanceGraph.getSize();
    }

    public Evaluator(DistanceGraph distanceGraph){
        this.distanceGraph = distanceGraph;
        problemType = ProblemType.TSP;
        N = distanceGraph.getSize();
    }

    private float evaluateIntegerVector(IntegerVector integerVector){
        float score = 0;
        switch (problemType){
            case QAP:
                score = calculateTotalQAPCost(integerVector);
            break;
            case TSP:
                score = distanceGraph.calculateTotalDistance(integerVector);
            break;
            default:
                throw new IllegalStateException("Unexpected value of problem type: " + problemType);
        }
        return score;
    }


    private float calculateTotalQAPCost(IntegerVector integerVector){
        float result =0.0f;

        for(int i = 0; i < N; i++){
            for(int j = 0; j < N; j++){
                float flow = flowsGraph.getEdge(i,j);
                int edgeRightEnd = integerVector.getElementAt(j);
                int edgeLeftEnd = integerVector.getElementAt(i);
                float dist = distanceGraph.getEdge(edgeLeftEnd,edgeRightEnd);
                result+=flow*dist;
            }
        }
        return result;
    }

    private float evaluateDescendingSequence(DescendingSequence sequence){
        return evaluateIntegerVector(sequence.toIntegerVector());
    }

    private float evaluateOrdinal(OrdinalNumber ordinal){
        return evaluateIntegerVector(ordinal.toIntegerVector());
    }

    public float evaluateSpecimen(SpecimenRepresentation specimen){
        switch(SpecimenRepresentationType.defineType(specimen.getClass())){
            case INTEGER_VECTOR:
                return evaluateIntegerVector((IntegerVector) specimen);

            case DESCENDING_SEQUENCE:
                return evaluateDescendingSequence((DescendingSequence) specimen);

            case ORDINAL_NUMBER:
                return evaluateOrdinal((OrdinalNumber) specimen);

            default:
                return 0.0f;
        }
    }

    public float inverseEvaluateSpecimen(SpecimenRepresentation specimen){
        return 1.0f/evaluateSpecimen(specimen);
    }


}