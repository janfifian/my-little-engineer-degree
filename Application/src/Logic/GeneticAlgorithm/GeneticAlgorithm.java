package Logic.GeneticAlgorithm;

import Data.InputSpecification.InputParameters;
import Data.SpecimenSpecification.Population;
import Data.SpecimenSpecification.SpecimenRepresentation;
import Data.SpecimenSpecification.SpecimenRepresentationType;
import GUI.MainGUIFrame;
import GUI.Reporter;
import Logic.GeneticAlgorithm.StopConditions.EvaluationFunctionCondition;
import Logic.GeneticAlgorithm.StopConditions.IterationCondition;
import Logic.GeneticAlgorithm.StopConditions.StopCondition;
import Logic.GeneticAlgorithm.StopConditions.TimeCondition;

import javax.swing.*;
import java.io.IOException;

public class GeneticAlgorithm {
    private SpecimenRepresentationType type;
    private String presetsDescription;
    private Population currentPopulation;
    private Evaluator evaluator;
    private Breeder breeder;
    private Mutator mutator;
    private Selector selector;
    protected StopCondition stopCondition;
    protected int iteration = 0;
    private int influxSize;
    private long time;
    private int populationSize;
    private float bestSpecimenScore=Float.MAX_VALUE;
    private String bestSpecimen = "";
    private PopulationGenerator populationGenerator;
    private int problemSize;
    private long startOfGeneticCycle;

    public GeneticAlgorithm(InputParameters inputParameters){
         type = inputParameters.getSpecimenRepresentation();
         this.breeder = new Breeder(inputParameters.getCrossoverOperator());
         this.mutator = new Mutator(inputParameters.getMutationOperator());
         this.selector = new Selector(inputParameters.getSelectionOperator());
         this.stopCondition = inputParameters.getStopCondition();
         this.evaluator = inputParameters.getEvaluator();
         populationSize = inputParameters.getPopulationSize();
        this.problemSize = inputParameters.getEvaluator().getProblemSize();
        this.populationGenerator = new PopulationGenerator(type, problemSize);
        this.influxSize = inputParameters.getInfluxSize();
        this.presetsDescription = inputParameters.getPresetsAsString();
    }

    public void execute(MainGUIFrame guiFrame){
        long generatingPopulationStart = System.currentTimeMillis();
        this.currentPopulation = populationGenerator.generateInitialPopulation(populationSize);
        startOfGeneticCycle = System.currentTimeMillis();
        performGeneticCycle(guiFrame);
        long endOfGeneticCycle = System.currentTimeMillis();
        iteration = 1;
        long step = (endOfGeneticCycle - startOfGeneticCycle);
        time=step;
        SwingUtilities.invokeLater(new Runnable() {
            public void run() {

        guiFrame.log("\n The initial population was generated in " + (startOfGeneticCycle - generatingPopulationStart)*0.001f + " seconds.");
        guiFrame.log("\n Initial population size: " + currentPopulation.getPopulation().size());
        if(stopCondition.getClass() == IterationCondition.class) {
            guiFrame.log("\n The first iteration of genetic algorithm was calculated in " + step*0.001f + " seconds.");
            guiFrame.log("\n Estimated duration of calculations: " + step*((IterationCondition) stopCondition).getIterationsToBeDone()*0.001f+ " seconds.");
            guiFrame.setExpectedIterationsLimit(((IterationCondition) stopCondition).getIterationsToBeDone());
        }
        else if(stopCondition.getClass() == TimeCondition.class){
            guiFrame.log("\n The first iteration of genetic algorithm was calculated in " + step*0.001f + " seconds.");
            guiFrame.setExpectedIterationsLimit((int) ((TimeCondition) stopCondition).getTimeRemaining());
                }
            }
        });
        while(true){
            try {
                if (stopCondition.checkWhetherSatisfied(iteration, bestSpecimenScore, time))
                    break;
            } catch (EvaluationFunctionCondition.UnableToSatisfyException e) {
                e.printStackTrace();
                guiFrame.log(e.getMessage());
                guiFrame.log("\n The algorithm was unable to find the solution satisfying the given constraints in hardcoded time and iterations limit.");
                guiFrame.log("The best specimen provided by the algorithm is the permutation of form \n\n"+bestSpecimen);
            }
        startOfGeneticCycle = System.currentTimeMillis();
        performGeneticCycle(guiFrame);
        iteration++;
        endOfGeneticCycle = System.currentTimeMillis();
        time+=(endOfGeneticCycle - startOfGeneticCycle);
        }
        guiFrame.log("The best specimen provided by the algorithm is the permutation of form \n"+bestSpecimen+",\n which has the total score"+bestSpecimenScore);
        String reportFileName = java.time.LocalDate.now().toString()+ " -- QuAdruPedal report.txt";
        Reporter reporter = new Reporter(reportFileName);
        try {
            reporter.saveReport(presetsDescription+"\n Total time: "+time, currentPopulation.getPopulation(), evaluator);
            guiFrame.log("Report saved as "+reportFileName);
        }
        catch(IOException ex){
            guiFrame.log("The application was unable to save a report due to the following exception: ");
            guiFrame.log("\n\n");
            guiFrame.log(ex.getMessage());
        }
        guiFrame.updateProgressBar(guiFrame.getExpectedIterationsLimit());
    }

    private void performGeneticCycle(MainGUIFrame guiFrame){
        /**
         * Performing the crossover as the first phase.
         */
        populationGenerator.generateInflux(currentPopulation,influxSize);
        currentPopulation = breeder.totalCrossover(currentPopulation);
        /**
         * Performing the mutations as the second phase.
         */
        mutator.mutatePopulation(currentPopulation);
        /**
         * Performing the selection as the last phase and picking up the best specimen.
         */
        SpecimenRepresentation selectionResult = selector.performSelection(currentPopulation);
        float potentialNewBestScore = evaluator.evaluateSpecimen(selectionResult);
        if(potentialNewBestScore<bestSpecimenScore){
            bestSpecimen = selectionResult.toString();
            bestSpecimenScore = potentialNewBestScore;
        }
        SwingUtilities.invokeLater(new Runnable() {
            public void run() {
                if(stopCondition.getClass() == IterationCondition.class) {
                    guiFrame.updateProgressBar(iteration);
                }
                else if(stopCondition.getClass() == TimeCondition.class){
                    guiFrame.updateProgressBar((int) time);
                }
            }
        });
    }
}
