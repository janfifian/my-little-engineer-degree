package Logic.GeneticAlgorithm;

import Data.SpecimenSpecification.Population;
import Data.SpecimenSpecification.SpecimenRepresentation;
import Logic.Operators.SelectionOperators.SelectionOperator;

/**
 * Class responsible for performing a selection during each iteration of genetic algorithm.
 */
public class Selector {
    /**
     * A chosen instance of selection operator, implementing the desired selection technique.
     */
    private SelectionOperator selectionOperator;

    /**
     * Constructs the selector object with the desired selection operator.
     * @param selectionOperator - chosen instance of selection operator, implementing the desired selection technique.
     */
    public Selector(SelectionOperator selectionOperator){
        this.selectionOperator = selectionOperator;
    }

    /**
     * This method puts to use the selection operator picked by user. To ensure that best specimen is selected  properly,
     * restrict only to those operators, which result in sorted population after the selection.
     * @param Pop - population on which the selection is to be performed
     * @return Pair consisting of population of selected specimens and the best specimen after the selection.
     */
    public SpecimenRepresentation performSelection(Population Pop){
        selectionOperator.performSelection(Pop);
        return (SpecimenRepresentation) Pop.getPopulation().get(0);
    }
}
