package Logic.Operators.CrossoverOperators;

public enum CrossoverOperationType {
    MPX("Maximal Preservation Crossover operator", new MaximalPreservationCrossover()),
    COMPX("Composition operator", new CompositionCrossover()),
    MEAN("Mean Value operator", new OrdinalNumberMean()),
    OPC("One-Point Crossover operator", new OnePointCrossover()),
    TPC("Two-Point Crossover operator", new TwoPointCrossover()),
    UC("Uniform Crossover operator", new UniformCrossover());

    private final String text;
    private final CrossoverOperator crossoverOperator;

    /**
     * @param text
     */
    CrossoverOperationType(final String text, CrossoverOperator crossoverOperator) {
        this.text = text;
        this.crossoverOperator = crossoverOperator;
    }

    public CrossoverOperator getOperator(){
        return this.crossoverOperator;
    }

    /* (non-Javadoc)
     * @see java.lang.Enum#toString()
     */
    @Override
    public String toString() {
        return text;
    }
}