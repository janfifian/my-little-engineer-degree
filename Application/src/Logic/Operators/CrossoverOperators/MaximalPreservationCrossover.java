package Logic.Operators.CrossoverOperators;

import Data.SpecimenSpecification.IntegerVector;
import Data.SpecimenSpecification.SpecimenRepresentation;
import Data.SpecimenSpecification.SpecimenRepresentationType;

import java.util.HashMap;
import java.util.Random;

public class MaximalPreservationCrossover extends CrossoverOperator {
    private static Random rand = new Random();

    /**
     * Sets the given seed for random number generator.
     * @param seed - the desired generator seed.
     */
    public void setSeed(long seed) {
        rand.setSeed(seed);
    }

    @Override
    public SpecimenRepresentation generateChild(SpecimenRepresentation A, SpecimenRepresentation B) {

            int n = ((IntegerVector) A).getSize();
            HashMap<Integer, Integer> offspring=new HashMap<>();
            int k = rand.nextInt(n);
            int l = rand.nextInt(n/2);
            /**
             * Standard rewriting operation, starting from k and ending at l
             */
            if(k+l<n){
                for(int i = k; i < k+l; i++){
                    offspring.put(i, ((IntegerVector) A).getElementAt(i));
                }
            } else {
                l = k+l-n;
                for(int i = 0; i < l; i++){
                    offspring.put(i, ((IntegerVector) A).getElementAt(i));
                }
                for(int i = k; i < n; i++){
                    offspring.put(i, ((IntegerVector) A).getElementAt(i));
                }
            }
            int index = 0;
            for(int j = 0; j < ((IntegerVector) B).getSize();j++){
                /**
                 * Move to the next empty spot
                 */
                while(offspring.containsKey(index)){
                    index++;
                };
                /**
                 * Check whether the child already has the nextChromosome of B inserted, if not - insert it in the first appropriate place
                 */
                Integer nextChromosome = ((IntegerVector) B).getElementAt(j);
                if(!offspring.containsValue(nextChromosome)){
                    offspring.put(index, nextChromosome);
                }
            }
            return new IntegerVector(offspring);
        }

    @Override
    public boolean isCompatible(SpecimenRepresentationType type) {
        if(type==SpecimenRepresentationType.INTEGER_VECTOR) return true;
        else
            return false;
    }

    @Override
    public boolean isSymmetric() {
        return false;
    }
}
