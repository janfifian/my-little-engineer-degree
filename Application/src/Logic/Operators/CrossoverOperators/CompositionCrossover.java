package Logic.Operators.CrossoverOperators;

import Data.SpecimenSpecification.IntegerVector;
import Data.SpecimenSpecification.SpecimenRepresentation;
import Data.SpecimenSpecification.SpecimenRepresentationType;

import java.util.HashMap;

/**
 * A crossover operator performing a composition on two permutations.
 * Suitable only for IntegerVector representation.
 */
public class CompositionCrossover extends CrossoverOperator {

    /**
     * Performs a mathematical superposition of two permutations. The resulting child is defined as the function C,
     * satisfying the equation C(n) = A(B(n))
     * @param A - IntegerVector representing LHS of the superposition
     * @param B - IntegerVector representing RHS of the superposition
     * @return a composition A o B
     */
    @Override
    public SpecimenRepresentation generateChild(SpecimenRepresentation A, SpecimenRepresentation B){
        HashMap<Integer, Integer> offspring = new HashMap<>();
        for (int i = 0; i < ((IntegerVector) A).getSize(); i++) {
            offspring.put(i, ((IntegerVector) A).getElementAt(((IntegerVector) B).getElementAt(i)));
        }
        return new IntegerVector(offspring);
    }

    @Override
    public boolean isCompatible(SpecimenRepresentationType type){
        if(type == SpecimenRepresentationType.INTEGER_VECTOR) return true;
        else
            return false;
    }

    @Override
    public boolean isSymmetric() {
        return false;
    }

}
