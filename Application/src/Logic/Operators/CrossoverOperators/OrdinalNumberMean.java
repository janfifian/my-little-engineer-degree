package Logic.Operators.CrossoverOperators;

import Data.SpecimenSpecification.OrdinalNumber;
import Data.SpecimenSpecification.SpecimenRepresentation;
import Data.SpecimenSpecification.SpecimenRepresentationType;

import java.math.BigInteger;

/**
 * A crossover operator performing the mean operation on two ordinal representations of permutation.
 * Suitable only for ordinal representations.
 */
public class OrdinalNumberMean extends CrossoverOperator {
    /**
     * Constructs a new OrdinalNumberMean crossover operator.
     */
    public OrdinalNumberMean(){}
    /**
     * Calculates the mean of two permutations. This operator does not depend on the order of its arguments.
     * @param A - first ordinal-represented permutation
     * @param B - second ordinal-represented permutation
     * @return Mean of two ordinal representations.
     */
    @Override
    public SpecimenRepresentation generateChild(SpecimenRepresentation A, SpecimenRepresentation B) {
            return new OrdinalNumber(((OrdinalNumber) A).getBigOrdinal().add(((OrdinalNumber) B).
                    getBigOrdinal()).divide(BigInteger.valueOf(2)));
    }

    @Override
    public boolean isCompatible(SpecimenRepresentationType type) {
        if(type == SpecimenRepresentationType.ORDINAL_NUMBER) return true;
        else
            return false;
    }

    @Override
    public boolean isSymmetric() {
        return true;
    }
}
