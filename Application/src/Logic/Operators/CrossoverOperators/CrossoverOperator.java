package Logic.Operators.CrossoverOperators;

import Data.SpecimenSpecification.SpecimenRepresentation;
import Data.SpecimenSpecification.SpecimenRepresentationType;
import Logic.Operators.ImproperOperatorException;

/**
 * Abstract class for crossover operators.
 */
public abstract class CrossoverOperator {
    /**
     * Performs a crossover of two specimen.
     * @param A - first specimen
     * @param B - second specimen
     * @return An offspring of (A,B) - note that in most of the cases the order of arguments does matter.
     * @throws ImproperOperatorException - if the operator is not suitable for provided specimen representations
     */
    public abstract SpecimenRepresentation generateChild(SpecimenRepresentation A, SpecimenRepresentation B) throws ImproperOperatorException;

    /**
     * Checks whether a given crossover operator is suitable for the provided representation.
     * @param type - type of permutation representation.
     * @return true if the operator can be used on specimens with given representation, false otherwise.
     */
    public abstract boolean isCompatible(SpecimenRepresentationType type);

    /**
     * Returns the information whether crossover operator is symmetric, i.e. whether arguments (A,B)
     * yield the same offspring as (B,A).
     * @return true if child of (A,B) equals the child of (B,A), false otherwise
     */
    public abstract boolean isSymmetric();
}
