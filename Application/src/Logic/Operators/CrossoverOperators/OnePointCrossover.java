package Logic.Operators.CrossoverOperators;

import Data.SpecimenSpecification.DescendingSequence;
import Data.SpecimenSpecification.SpecimenRepresentation;
import Data.SpecimenSpecification.SpecimenRepresentationType;
import Logic.Operators.ImproperOperatorException;

import java.util.ArrayList;
import java.util.Random;

public class OnePointCrossover extends CrossoverOperator {
    private Random RNG = new Random(System.currentTimeMillis());

    @Override
    public SpecimenRepresentation generateChild(SpecimenRepresentation A, SpecimenRepresentation B) throws ImproperOperatorException {
        ArrayList<Integer> child = new ArrayList<>();
        int n = ((DescendingSequence) A).getSequence().size();
        int k = RNG.nextInt(n);
        for(int i = 0; i < k; i++){
            child.add(((DescendingSequence) A).getSequence().get(i));
        }
        for(int i = k; i < n; i++){
            child.add(((DescendingSequence) B).getSequence().get(i));
        }
        return new DescendingSequence(child);
    }

    @Override
    public boolean isCompatible(SpecimenRepresentationType type) {
        if(type == SpecimenRepresentationType.DESCENDING_SEQUENCE) return true;
        else
            return false;
    }

    @Override
    public boolean isSymmetric() {
        return false;
    }

    public void setRandomSeed(long seed){
        RNG.setSeed(seed);
    }
}
