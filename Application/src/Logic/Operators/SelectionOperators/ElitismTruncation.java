package Logic.Operators.SelectionOperators;

import Data.SpecimenSpecification.Population;
import Data.SpecimenSpecification.SpecimenRepresentation;

public class ElitismTruncation extends SelectionOperator {

    private int percentage;

    public ElitismTruncation(int percentage){
        this.percentage = percentage;
    }

    @Override
    public String toString(){
        return "Truncation Selection";
    }

    @Override
    public <T extends SpecimenRepresentation> void performSelection(Population<T> pop) {
        int n=pop.getPopulation().size();
        pop.sortPopulation();
        pop.getPopulation().subList(populationSize,n).clear();
        for(int i =0; i < percentage*populationSize/100; i++ ){
            pop.getPopulation().get(i).setMutability(false);
        }
    }
}

