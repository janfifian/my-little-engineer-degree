package Logic.Operators.SelectionOperators;

public enum SelectionOperatorType {
    TRUNCATION("Truncation", new Truncation()),
    ELITISM10PERCENT("Truncation, 10% elitism", new ElitismTruncation(10)),
    ELITISM15PERCENT("Truncation, 15% elitism", new ElitismTruncation(15)),
    ELITISM20PERCENT("Truncation, 20% elitism", new ElitismTruncation(20)),
    ROULETTE("Roulette wheel selection", new RouletteWheelSelection(false,0)),
    ELITIST10ROULETTE("RW selection, 10% elitism", new RouletteWheelSelection(true,10)),
    ELITIST15ROULETTE("RW selection, 15% elitism", new RouletteWheelSelection(true,15)),
    ELITIST20ROULETTE("RW selection, 20% elitism", new RouletteWheelSelection(true,20));

    private final String text;
    private final SelectionOperator selectionOperator;

    /**
     * @param text
     */
    SelectionOperatorType(final String text, SelectionOperator selectionOperator) {
        this.text = text;
        this.selectionOperator = selectionOperator;
    }
    public SelectionOperator getOperator(){
        return this.selectionOperator;
    }

    /* (non-Javadoc)
     * @see java.lang.Enum#toString()
     */
    @Override
    public String toString() {
        return text;
    }
}