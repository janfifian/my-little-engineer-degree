package Logic.Operators.SelectionOperators;

import Data.SpecimenSpecification.Population;
import Data.SpecimenSpecification.SpecimenRepresentation;
import Logic.GeneticAlgorithm.Evaluator;

import java.util.ArrayList;
import java.util.Random;

public class RouletteWheelSelection extends SelectionOperator {

    private int percentage;
    private boolean elitist;

    public RouletteWheelSelection(boolean isElitist, int percentage){
        this.elitist=isElitist;
        this.percentage = percentage;
    }

    private int bestSpecimenIndex;

    private Random RNG = new Random(System.currentTimeMillis());

    public void setSeed(long seed){
        RNG.setSeed(seed);
    }

    private ArrayList<Float> CDF;

    private float sum;

    public <T extends SpecimenRepresentation> void calculateCDF(Population<T> pop){
        Evaluator eval = pop.getComparator().getEvaluator();
        CDF = new ArrayList<>(pop.getPopulation().size());
        float bestOne = eval.inverseEvaluateSpecimen(pop.getPopulation().get(0));
        CDF.add(bestOne);
        bestSpecimenIndex = 0;
        float concurrency;
        for(int i = 1; i < pop.getPopulation().size(); i++){
            concurrency =  eval.inverseEvaluateSpecimen(pop.getPopulation().get(i));
            CDF.add(concurrency +CDF.get(i-1));
            if(concurrency>bestOne){
                bestSpecimenIndex = i;
            }
        }
        sum = CDF.get(CDF.size()-1);
    }

    public ArrayList<Float> getCDF(){
        return CDF;
    }

    public int getIndexFromCDF(float argument){
        int l = 0, r = CDF.size() - 1;
        while (l <= r) {
            int m = l + (r - l) / 2;
            if (CDF.get(m) < argument) {
                l = m + 1;
            }
            else {
                r = m - 1;
            }
        }
        return l;
    }

    @Override
    public <T extends SpecimenRepresentation> void performSelection(Population<T> pop) {
        Population<T> newPop = new Population<>();
        if(elitist){
            pop.sortPopulation();
            calculateCDF(pop);
            for(int i = 0; i < populationSize*percentage/100; i++){
                newPop.addSpecimen(pop.getPopulation().get(i));
                newPop.getPopulation().get(i).setMutability(false);
            }
            float point = RNG.nextFloat() * sum;
            for (int i = newPop.getPopulation().size(); i < populationSize; i++) {
                newPop.addSpecimen(pop.getPopulation().get(getIndexFromCDF(point)));
                point = RNG.nextFloat() * sum;
            }
        }
        else
        {
            calculateCDF(pop);
            newPop.addSpecimen(pop.getPopulation().get(bestSpecimenIndex));
            float point = RNG.nextFloat() * sum;
            for (int i = 1; i < populationSize; i++) {
                newPop.addSpecimen(pop.getPopulation().get(getIndexFromCDF(point)));
                point = RNG.nextFloat() * sum;
            }

        }

        pop.setPopulation(newPop);
    }
}
