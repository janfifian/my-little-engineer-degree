package Logic.Operators.SelectionOperators;

import Data.SpecimenSpecification.Population;
import Data.SpecimenSpecification.SpecimenRepresentation;

public class Truncation extends SelectionOperator {

    @Override
    public String toString(){
        return "Truncation Selection";
    }

    @Override
    public <T extends SpecimenRepresentation> void performSelection(Population<T> pop) {
        int n=pop.getPopulation().size();
        pop.sortPopulation();
        pop.getPopulation().subList(populationSize,n).clear();
    }
}
