package Logic.Operators.SelectionOperators;

import Logic.GeneticAlgorithm.Evaluator;
import Data.SpecimenSpecification.Population;
import Data.SpecimenSpecification.SpecimenRepresentation;

public abstract class SelectionOperator {
    protected Evaluator evaluator;
    protected int populationSize;


    public abstract <T extends SpecimenRepresentation> void performSelection(Population<T> pop);

    public void setEvaluator(Evaluator evaluator) {
        this.evaluator = evaluator;
    }
    public void setPopulationSize(int populationSize){
        this.populationSize = populationSize;
    }
}
