package Logic.Operators.MutationOperators;

import Data.SpecimenSpecification.*;

import java.math.BigInteger;

/**
 * Performs a random shift on a single position in the case of descending sequence representation
 * or shifts the index in the lexicographic order if the representation is the OrdinalNumber.
 */
public class ShiftOperator extends MutationOperator {
    @Override
    public void mutate(SpecimenRepresentation specimen) {
        switch(SpecimenRepresentationType.defineType(specimen.getClass())){
            case DESCENDING_SEQUENCE:
               int p = ShiftOperator.randomDistribution.generateMutationsNumber();
               for(int i = 0; i < p; i++){
                   int pos =  ShiftOperator.randomDistribution.generateRandomNumberFromRange(0,DescendingSequence.problemSize-1);
                   int val = ShiftOperator.randomDistribution.generateRandomNumberFromRange(0,(DescendingSequence.problemSize-1)-pos);
                   ((DescendingSequence) specimen).getSequence().set(pos,val);
               }
            break;

            case ORDINAL_NUMBER:
                int k = OrdinalNumber.getProblemSize()/2;
                int l = randomDistribution.generateRandomNumberFromRange(0,k+1);
                BigInteger newOrdinal = new BigInteger(OrdinalNumber.precalculatedFactorials.get(l).bitLength(),randomDistribution.getRandomEngine());
                ((OrdinalNumber) specimen).setBigOrdinal(((OrdinalNumber) specimen).getBigOrdinal()
                        .add(newOrdinal).mod(OrdinalNumber.precalculatedFactorials
                        .get(OrdinalNumber.getProblemSize())));
            break;
        }
    }

    @Override
    public boolean isCompatible(SpecimenRepresentationType specType) {
        if(specType == SpecimenRepresentationType.DESCENDING_SEQUENCE || specType == SpecimenRepresentationType.ORDINAL_NUMBER) return true;
        else
            return false;
    }
}
