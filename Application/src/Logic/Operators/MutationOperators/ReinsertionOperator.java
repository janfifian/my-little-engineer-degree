package Logic.Operators.MutationOperators;

import Data.SpecimenSpecification.IntegerVector;
import Data.SpecimenSpecification.SpecimenRepresentation;
import Data.SpecimenSpecification.SpecimenRepresentationType;

public class ReinsertionOperator extends MutationOperator {
    @Override
    public void mutate(SpecimenRepresentation specimen) {
        int p = ShiftOperator.randomDistribution.generateMutationsNumber();
        int n = ((IntegerVector) specimen).getSize();
        while(p-->0) {
            int a = ShiftOperator.randomDistribution.generateRandomNumberFromRange(0, n - 3);
            int b = ShiftOperator.randomDistribution.generateRandomNumberFromRange(a+1, n - 2);
            int c = ShiftOperator.randomDistribution.generateRandomNumberFromRange(b+1, n-1);
            int x = ((IntegerVector) specimen).getElementAt(a);
            int y = ((IntegerVector) specimen).getElementAt(b);
            int z = ((IntegerVector) specimen).getElementAt(c);
            ((IntegerVector) specimen).setElementAt(b, x);
            ((IntegerVector) specimen).setElementAt(c, y);
            ((IntegerVector) specimen).setElementAt(a, z);
        }
    }

    @Override
    public boolean isCompatible(SpecimenRepresentationType specType) {
        if(specType == SpecimenRepresentationType.INTEGER_VECTOR){
            return true;
        }
        else
            return false;
    }
}

