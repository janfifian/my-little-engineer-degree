package Logic.Operators.MutationOperators;

import Data.SpecimenSpecification.DescendingSequence;
import Data.SpecimenSpecification.IntegerVector;
import Data.SpecimenSpecification.SpecimenRepresentation;
import Data.SpecimenSpecification.SpecimenRepresentationType;

public class SwapOperator extends MutationOperator {
    @Override
    public void mutate(SpecimenRepresentation specimen) {
        int p = SwapOperator.randomDistribution.generateMutationsNumber();
        int n;
        switch(SpecimenRepresentationType.defineType(specimen.getClass())){
            case DESCENDING_SEQUENCE:
                n = ((DescendingSequence) specimen).getSequence().size();
                while(p-->0){
                    int a= ShiftOperator.randomDistribution.generateRandomNumberFromRange(0,n-2);
                    int b= ShiftOperator.randomDistribution.generateRandomNumberFromRange(a+1,n-1);
                    int q=((DescendingSequence) specimen).getSequence().get(b);
                    int z=((DescendingSequence) specimen).getSequence().get(a) % (n-1-b);
                    ((DescendingSequence) specimen).getSequence().set(b,z);
                    ((DescendingSequence) specimen).getSequence().set(a,q);
                }
                break;
            case INTEGER_VECTOR:
                n = ((IntegerVector) specimen).getSize();
                while(p-->0){
                int a= ShiftOperator.randomDistribution.generateRandomNumberFromRange(0,n);
                int b= ShiftOperator.randomDistribution.generateRandomNumberFromRange(0,n);
                int q=((IntegerVector) specimen).getElementAt(b);
                ((IntegerVector) specimen).setElementAt(b,((IntegerVector) specimen).getElementAt(a));
                ((IntegerVector) specimen).setElementAt(a,q);
                }
            break;
        }
    }

    @Override
    public boolean isCompatible(SpecimenRepresentationType specType) {
        if(specType == SpecimenRepresentationType.INTEGER_VECTOR || specType == SpecimenRepresentationType.DESCENDING_SEQUENCE) return true;
        else
            return false;
    }
}
