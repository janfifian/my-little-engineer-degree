package Logic.Operators.MutationOperators;

public enum MutationOperatorType {
    SWAP("Swap operator", new SwapOperator()),
    SHIFT("Shift operator", new ShiftOperator()),
    REINSERT("Reinsertion operator", new ReinsertionOperator())
        ;

private final String text;
private final MutationOperator mutationOperator;

        /**
         * @param text
         */
        MutationOperatorType(final String text, MutationOperator mutationOperator) {
        this.text = text;
        this.mutationOperator = mutationOperator;
        }

        public MutationOperator getOperator(){
            return this.mutationOperator;
        }
/* (non-Javadoc)
 * @see java.lang.Enum#toString()
 */
@Override
public String toString() {
        return text;
        }
}
