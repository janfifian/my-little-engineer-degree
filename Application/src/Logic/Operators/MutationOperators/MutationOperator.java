package Logic.Operators.MutationOperators;

import Data.Distributions.RandomDistribution;
import Data.SpecimenSpecification.SpecimenRepresentation;
import Data.SpecimenSpecification.SpecimenRepresentationType;


public abstract class MutationOperator {
    protected static RandomDistribution randomDistribution;
    public abstract void mutate(SpecimenRepresentation specimen);
    public abstract boolean isCompatible(SpecimenRepresentationType specType);
    public static void setRandomDistribution(RandomDistribution dist){
        randomDistribution = dist;
    }
}
