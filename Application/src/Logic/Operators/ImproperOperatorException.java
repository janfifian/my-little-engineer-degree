package Logic.Operators;

public class ImproperOperatorException extends Exception {
    private final static String errorMessage = "The following operator is not suitable for the given permutation representation!";
    public ImproperOperatorException(String s){
        super(s);
    }

    public ImproperOperatorException() {
        super(errorMessage);
    }
}
