package Test.MockClasses;

import Data.InputSpecification.CSVReader;
import Data.InputSpecification.InputParameters;
import Data.ProblemSpecification.DistanceGraph;
import Data.ProblemSpecification.ProblemType;
import GUI.MainGUIFrame;
import Logic.GeneticAlgorithm.Evaluator;
import Logic.GeneticAlgorithm.GeneticAlgorithm;
import Logic.GeneticAlgorithm.StopConditions.EvaluationFunctionCondition;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

public class MockGeneticCycleClass extends GeneticAlgorithm {

    public double getBestSpec() {
        return bestSpec;
    }

    public void setBestSpec(double bestSpec) {
        this.bestSpec = bestSpec;
    }

    private double bestSpec;

    public long getTimeInMillis() {
        return timeInMillis;
    }

    public void setTimeInMillis(long timeInMillis) {
        this.timeInMillis = timeInMillis;
    }

    private long timeInMillis;

    public MockGeneticCycleClass(InputParameters inputParameters) {
        super(inputParameters);
    }

    public int getIteration(){
        return this.iteration;
    }

    private float step = 1.0f;

    public void setStep(float step) {
        this.step = step;
    }

    @Override
    public void execute(MainGUIFrame frame){
        bestSpec = 400.0f;
        timeInMillis=0;
        this.iteration = 0;
        long startTimeInMillis=System.currentTimeMillis();
        while (true) {
            try {
                if (this.stopCondition.checkWhetherSatisfied(this.iteration,bestSpec,timeInMillis)) break;
            } catch (EvaluationFunctionCondition.UnableToSatisfyException e) {
                e.printStackTrace();
            }
            timeInMillis = timeInMillis + System.currentTimeMillis()-startTimeInMillis;
            startTimeInMillis=System.currentTimeMillis();
            bestSpec-=step;
            this.iteration++;
        }
    }
}
