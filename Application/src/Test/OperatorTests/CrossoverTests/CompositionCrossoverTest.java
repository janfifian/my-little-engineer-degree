package Test.OperatorTests.CrossoverTests;

import Data.SpecimenSpecification.IntegerVector;
import Logic.Operators.CrossoverOperators.CompositionCrossover;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Arrays;

import static org.junit.jupiter.api.Assertions.*;

class CompositionCrossoverTest {

    private static CompositionCrossover composer = new CompositionCrossover();
    @Test
    void shouldPerformAppropriateCompositionInEachWayOfTwoGivenPermutations() {

        IntegerVector A = new IntegerVector(new ArrayList<Integer>(Arrays.asList(2,1,3,0) ));
        IntegerVector B = new IntegerVector(new ArrayList<Integer>(Arrays.asList(3,2,1,0) ));
        IntegerVector C = (IntegerVector) composer.generateChild(A,B);
        IntegerVector D = (IntegerVector) composer.generateChild(B,A);

        assertEquals(0,C.getElementAt(0));
        assertEquals(3,C.getElementAt(1));
        assertEquals(1,C.getElementAt(2));
        assertEquals(2,C.getElementAt(3));

        assertEquals(1,D.getElementAt(0));
        assertEquals(2,D.getElementAt(1));
        assertEquals(0,D.getElementAt(2));
        assertEquals(3,D.getElementAt(3));
    }
}