package Test.OperatorTests.CrossoverTests;

import Data.SpecimenSpecification.OrdinalNumber;
import Logic.Operators.CrossoverOperators.OrdinalNumberMean;
import org.junit.jupiter.api.Test;

import java.math.BigInteger;

import static org.junit.jupiter.api.Assertions.*;

class OrdinalNumberMeanTest {

    private static OrdinalNumberMean onm= new OrdinalNumberMean();
    @Test
    void shouldProperlyCalculateMeanAndGenerateAppropriateChild() {

        OrdinalNumber Ord1 = new OrdinalNumber(new BigInteger("10000"));
        OrdinalNumber Ord2 = new OrdinalNumber(new BigInteger("30000"));
        OrdinalNumber Ord12 = (OrdinalNumber) onm.generateChild(Ord1,Ord2);
        assertEquals(Ord12.getBigOrdinal(),new BigInteger("20000"));
    }
}