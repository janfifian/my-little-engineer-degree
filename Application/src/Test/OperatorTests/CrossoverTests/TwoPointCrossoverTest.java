package Test.OperatorTests.CrossoverTests;

import Data.SpecimenSpecification.DescendingSequence;
import Logic.Operators.ImproperOperatorException;
import Logic.Operators.CrossoverOperators.TwoPointCrossover;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Arrays;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class TwoPointCrossoverTest{

    private static TwoPointCrossover twoPointCrossover = new TwoPointCrossover();
    @Test
    void shouldPerformAppropriateCrossoverInEachWayOfTwoGivenPermutations() throws ImproperOperatorException {
        DescendingSequence.setProblemSize(8);
        twoPointCrossover.setRandomSeed(1);
        DescendingSequence A = new DescendingSequence(new ArrayList<Integer>(Arrays.asList(5,5,4,0,3,2,1,0)));
        DescendingSequence B = new DescendingSequence(new ArrayList<Integer>(Arrays.asList(3,3,1,0,3,2,1,0)));
        DescendingSequence C = (DescendingSequence) twoPointCrossover.generateChild(A,A);
        DescendingSequence D = (DescendingSequence) twoPointCrossover.generateChild(B,A);
        DescendingSequence E = (DescendingSequence) twoPointCrossover.generateChild(A,B);
        //Check whether crossovers have appropriate length
        assertEquals(8,C.getSequence().size());
        assertEquals(8,D.getSequence().size());
        assertEquals(8,E.getSequence().size());
        //Check whether AxA equals A
        assertEquals(A.getSequence(), C.getSequence());

        //Check whether first element is inherited from appropriate parent
        assertEquals(3,D.getSequence().get(0));
        assertEquals(5,E.getSequence().get(0));
    }
}