package Test.OperatorTests.MutationTests;

import Data.Distributions.OnePointDistribution;
import Data.Distributions.RandomDistribution;
import Data.SpecimenSpecification.DescendingSequence;
import Data.SpecimenSpecification.OrdinalNumber;
import Logic.Operators.MutationOperators.MutationOperator;
import Logic.Operators.MutationOperators.ShiftOperator;
import Logic.Operators.MutationOperators.SwapOperator;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Arrays;

import static org.junit.jupiter.api.Assertions.*;

public class MutationOperatorsTest {

    public static RandomDistribution rand;

    @BeforeEach
    void prepareEnvironment(){
        rand = new OnePointDistribution();
        ((OnePointDistribution) rand).setRandomSeed(1);
        OrdinalNumber.setProblemSize(17);
    }

    @Test
    void shouldMakeOrdinalRepresentationDifferentAfterShiftMutation(){
        OrdinalNumber ord = new OrdinalNumber(new BigInteger("100"));
        MutationOperator mut = new ShiftOperator();
        mut.setRandomDistribution(rand);
        BigInteger bigInteger = new BigInteger("100");
        mut.mutate(ord);
        assertNotEquals(bigInteger,ord.getBigOrdinal());
    }

    @Test
    void shouldMakeDescendingSequenceRepresentationDifferentAfterShiftMutation(){

        DescendingSequence.setProblemSize(7);
        DescendingSequence desc = new DescendingSequence(new ArrayList<Integer>(Arrays.asList(5,5,4,0,0,0,0)));
        //just FYI
        ArrayList<Integer> integerArrayListRepresentation = new ArrayList<>(Arrays.asList(5,6,4,0,1,2,3));
        assertEquals(desc.toArrayList(),integerArrayListRepresentation);
        MutationOperator mut = new ShiftOperator();
        mut.setRandomDistribution(rand);
        mut.mutate(desc);
        assertNotEquals(desc.toArrayList(),integerArrayListRepresentation);
    }

    @Test
    void shouldMakeDescendingSequenceRepresentationDifferentAfterSwapMutation(){
        DescendingSequence.setProblemSize(8);
        DescendingSequence desc = new DescendingSequence(new ArrayList<Integer>(Arrays.asList(5,5,4,0,3,2,1,0)));
        //just FYI
        ArrayList<Integer> integerArrayListRepresentation = new ArrayList<>(Arrays.asList(5, 6, 4, 0, 7, 3, 2, 1));
        assertEquals(integerArrayListRepresentation,desc.toArrayList());
        MutationOperator mut = new SwapOperator();
        mut.setRandomDistribution(rand);
        mut.mutate(desc);
        assertNotEquals(desc.toArrayList(),integerArrayListRepresentation);
    }

}
