package Test.OperatorTests.SelectionTests;

import Data.InputSpecification.CSVReader;
import Logic.GeneticAlgorithm.Evaluator;
import Data.SpecimenSpecification.IntegerVector;
import Data.SpecimenSpecification.Population;
import Logic.GeneticAlgorithm.SpecimenComparator;
import Logic.Operators.SelectionOperators.Truncation;
import Test.MockClasses.MockGUIFrame;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;

import static org.junit.jupiter.api.Assertions.*;

class TruncationTest {

    private static Evaluator eval;

    @BeforeEach
    void prepareEnvironment(){
        MockGUIFrame mockFrame = new MockGUIFrame();
        CSVReader csvReader = new CSVReader();
        try {
            eval = new Evaluator(csvReader.readGraph(new File("./src/Test/TestFiles/4VerticesTSP.csv"), mockFrame));
        } catch (IOException e) {
            e.printStackTrace();
        }
        Population.setSpecimenComparator(new SpecimenComparator(eval));
    }

    @Test
    void operator() {
        Truncation T = new Truncation();
        T.setEvaluator(eval);
        T.setPopulationSize(3);
        Population<IntegerVector> Pop = new Population<>();
        Pop.addSpecimen( new IntegerVector(new ArrayList<>(Arrays.asList(1, 3, 2, 0))));
        for(int i = 0; i < 10; i++) {
            Pop.addSpecimen(new IntegerVector(new ArrayList<>(Arrays.asList(0, 1, 2, 3))));
        }
        Pop.addSpecimen( new IntegerVector(new ArrayList<>(Arrays.asList(1, 3, 2, 0))));
        Pop.addSpecimen( new IntegerVector(new ArrayList<>(Arrays.asList(1, 3, 2, 0))));
        assertEquals(95,eval.evaluateSpecimen(Pop.getPopulation().get(3)));
        T.performSelection(Pop);
        assertEquals(3,Pop.getPopulation().size());
        assertEquals(80,eval.evaluateSpecimen(Pop.getPopulation().get(0)));
        assertEquals(80,eval.evaluateSpecimen(Pop.getPopulation().get(2)));
    }
}