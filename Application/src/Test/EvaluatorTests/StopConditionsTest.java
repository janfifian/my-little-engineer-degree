package Test.EvaluatorTests;

import Data.InputSpecification.InputParameters;
import Data.ProblemSpecification.ProblemType;
import Logic.GeneticAlgorithm.StopConditions.IterationCondition;
import Logic.GeneticAlgorithm.StopConditions.StopConditionType;
import Test.MockClasses.MockGUIFrame;
import Test.MockClasses.MockGeneticCycleClass;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.File;

import static org.junit.jupiter.api.Assertions.*;

class StopConditionsTest {
    static InputParameters params;
    static MockGUIFrame mockFrame;

    @BeforeEach
    void setUp() {
        params = new InputParameters();
        mockFrame = new MockGUIFrame();
        params.setEvaluator(ProblemType.TSP, new File("./src/Test/TestFiles/4VerticesTSP.csv"),mockFrame);
    }

    @Test
    void shouldStopWorkingAfter1000IterationsWithIterationStopCondition() {
        params.setStopCondition(StopConditionType.IterationLimitCondition,"1000");
        MockGeneticCycleClass cycler = new MockGeneticCycleClass(params);
        cycler.execute(mockFrame);
        assertEquals(1000,cycler.getIteration());
    }

    @Test
    void shouldStopWorkingAfterMoreThan2SecondsWithTimeStopCondition() {
        params.setStopCondition(StopConditionType.TimeLimitCondition,"5000");
        MockGeneticCycleClass cycler = new MockGeneticCycleClass(params);
        long StartTime = System.currentTimeMillis();
        cycler.execute(mockFrame);
        long resultingTime=System.currentTimeMillis() - StartTime;
        assertTrue(resultingTime >= 5000, "The time (" + resultingTime + ") should be greater than 5000 ms ");
    }

    @Test
    void shouldStopWorkingAfterTheBestSpecimenAttainsValueOf200() {
        params.setStopCondition(StopConditionType.FunctionValueCondition,"200");

        MockGeneticCycleClass cycler = new MockGeneticCycleClass(params);
        long StartTime = System.currentTimeMillis();
        cycler.execute(mockFrame);
        long resultingTime=System.currentTimeMillis() - StartTime;
        assertTrue(cycler.getBestSpec() <= 200, "The best specimen obtained (" + cycler.getBestSpec() + ") should have value which does not exceed 200");
    }
}