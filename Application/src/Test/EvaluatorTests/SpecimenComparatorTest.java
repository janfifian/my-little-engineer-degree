package Test.EvaluatorTests;

import Data.InputSpecification.CSVReader;
import Logic.GeneticAlgorithm.Evaluator;
import Data.SpecimenSpecification.*;
import Logic.GeneticAlgorithm.SpecimenComparator;
import Test.MockClasses.MockGUIFrame;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.File;
import java.io.IOException;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Arrays;

import static org.junit.jupiter.api.Assertions.*;

class SpecimenComparatorTest {
    private static Evaluator eval;

    @BeforeEach
    void prepareEnvironment(){
        MockGUIFrame mockFrame = new MockGUIFrame();
        CSVReader csvReader = new CSVReader();
        try {
            eval = new Evaluator(csvReader.readGraph(new File("./src/Test/TestFiles/4VerticesTSP.csv"), mockFrame));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Test
    void shouldProperlyCompareTwoSpecimenWhenRepresentedAsIntegerVectors(){
        SpecimenComparator sc = new SpecimenComparator(eval);
        IntegerVector A = new IntegerVector(new ArrayList<>(Arrays.asList(0, 1, 2, 3)));
        IntegerVector B = new IntegerVector(new ArrayList<>(Arrays.asList(1, 3, 2, 0)));
        assertEquals(1,sc.compare(A,B));
        assertEquals(-1,sc.compare(B,A));
        assertEquals(0,sc.compare(A,A));
    }

    @Test
    void shouldProperlyCompareTwoSpecimenWhenRepresentedAsOrdinalNumbers(){
        SpecimenComparator sc = new SpecimenComparator(eval);
        OrdinalNumber.setProblemSize(4);
        OrdinalNumber A = new OrdinalNumber(new BigInteger("0"));
        OrdinalNumber B = new OrdinalNumber(new BigInteger("11"));
        OrdinalNumber C = new OrdinalNumber(new BigInteger("1"));
        assertEquals(1,sc.compare(A,B));
        assertEquals(-1,sc.compare(B,A));
        assertEquals(0,sc.compare(C,B));
    }

    @Test
    void shouldProperlyCompareTwoSpecimenWhenRepresentedAsDescendingSequences(){
        SpecimenComparator sc = new SpecimenComparator(eval);
        DescendingSequence.setProblemSize(4);
        DescendingSequence A = new DescendingSequence(new ArrayList<>(Arrays.asList(0, 0, 0, 0)));
        DescendingSequence B = new DescendingSequence(new ArrayList<>(Arrays.asList(1, 2, 1, 0)));
        assertEquals(1,sc.compare(A,B));
        assertEquals(-1,sc.compare(B,A));
        assertEquals(0,sc.compare(A,A));
    }
}