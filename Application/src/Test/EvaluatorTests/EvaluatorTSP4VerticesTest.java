package Test.EvaluatorTests;

import Data.InputSpecification.CSVReader;
import Logic.GeneticAlgorithm.Evaluator;
import Data.SpecimenSpecification.IntegerVector;
import Test.MockClasses.MockGUIFrame;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;

import static org.junit.jupiter.api.Assertions.*;

class EvaluatorTSP4VerticesTest {

    private static MockGUIFrame  mockFrame;
    private static Evaluator eval;
    private static CSVReader csvReader;
    @BeforeEach
    void prepareEnvironment(){
        mockFrame= new MockGUIFrame();
        csvReader = new CSVReader();
        try {
            eval = new Evaluator(csvReader.readGraph(new File("./src/Test/TestFiles/4VerticesTSP.csv"),mockFrame));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Test
    void shouldReturnCorrectlyProblemSize() {
        assertEquals(4, eval.getProblemSize());
    }

    @Test
    void shouldCorrectlyReplaceGraphWithANewOne() {
        try {
            eval.setDistanceGraph(new File("./src/Test/TestFiles/5VerticesTSP.csv"),mockFrame);
        } catch (IOException e) {
            e.printStackTrace();
        }
        assertEquals(5,eval.getProblemSize());
    }

    @Test
    void shouldCorrectlyEvaluateTheSameSpecimenDespiteTheirDifferentFormAccordingToGivenGraph() {
        IntegerVector A = new IntegerVector(new ArrayList<Integer>(Arrays.asList(1,3,2,0)));
        IntegerVector B = new IntegerVector(new ArrayList<Integer>(Arrays.asList(3,2,0,1)));
        IntegerVector C = new IntegerVector(new ArrayList<Integer>(Arrays.asList(2,0,1,3)));
        IntegerVector D = new IntegerVector(new ArrayList<Integer>(Arrays.asList(0,1,3,2)));
        float[] scores = new float[]{(eval.evaluateSpecimen(A)), (eval.evaluateSpecimen(B)), (eval.evaluateSpecimen(C)),
                                     (eval.evaluateSpecimen(D))};
        java.util.Arrays.sort(scores);
        assertEquals(scores[0],scores[3]);
        assertEquals(80.0f,scores[0]);
    }
}