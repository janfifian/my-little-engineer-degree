package Test.EvaluatorTests;

import Data.InputSpecification.CSVReader;
import Data.InputSpecification.Pair;
import Data.ProblemSpecification.DistanceGraph;
import Data.ProblemSpecification.FlowsGraph;
import Data.SpecimenSpecification.IntegerVector;
import Logic.GeneticAlgorithm.Evaluator;
import Test.MockClasses.MockGUIFrame;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;

import static org.junit.jupiter.api.Assertions.assertEquals;

/**
 * Tests whether QAP graphs yield proper values for the given specimen.
 */
class EvaluatorQAP3VerticesTest {

    private static MockGUIFrame  mockFrame;
    private static Evaluator eval;
    private static CSVReader csvReader;
    @BeforeEach
    void prepareEnvironment(){
        mockFrame= new MockGUIFrame();
        csvReader = new CSVReader();
        try {
            Pair<DistanceGraph, FlowsGraph> QAPGraph = csvReader.readQAPGraph(new File("./src/Test/TestFiles/3VerticesQAP.csv"),mockFrame);
            eval = new Evaluator(QAPGraph.getSecond(),QAPGraph.getFirst());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Test
    void shouldReturnCorrectlyProblemSize() {
        assertEquals(3, eval.getProblemSize());
    }


    @Test
    void shouldCorrectlyEvaluateSpecimen() {
        IntegerVector A = new IntegerVector(new ArrayList<Integer>(Arrays.asList(2,0,1)));
        IntegerVector D = new IntegerVector(new ArrayList<Integer>(Arrays.asList(0,1,2)));
        float scoreA = eval.evaluateSpecimen(A);
        float scoreD = eval.evaluateSpecimen(D);
        assertEquals(17.0f,scoreD);
        assertEquals(14.0f,scoreA);
    }
}