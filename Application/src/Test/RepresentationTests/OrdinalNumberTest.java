package Test.RepresentationTests;

import Data.SpecimenSpecification.OrdinalNumber;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.math.BigInteger;
import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.*;

class OrdinalNumberTest {

    @BeforeEach
    void defineProblemSize() {
        OrdinalNumber.setProblemSize(4);
    }

    @Test
    void shouldProperlyConvertOrdinalNumberToStandardPermutationRepresentation() {
        OrdinalNumber ordinalNumber = new OrdinalNumber(new BigInteger("20"));
        ArrayList<Integer> integerArrayList = ordinalNumber.toIntegerArrayList();
        assertEquals(3,integerArrayList.get(0));
        assertEquals(1,integerArrayList.get(1));
        assertEquals(0,integerArrayList.get(2));
        assertEquals(2,integerArrayList.get(3));
    }
}