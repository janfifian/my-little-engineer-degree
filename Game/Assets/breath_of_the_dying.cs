﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class breath_of_the_dying : MonoBehaviour {

    AudioSource audiosource;
	// Use this for initialization
	void Start () {
        this.audiosource = this.transform.GetComponent<AudioSource>();
        string path = "Sounds/DeathSounds/" + Random.Range(1, 5);
        audiosource.clip = (AudioClip)Resources.Load(path, typeof(AudioClip));
        StartCoroutine(death());
    }

    IEnumerator death()
    {
        audiosource.Play();
        yield return new WaitForSeconds(audiosource.clip.length);
        Destroy(this.transform.gameObject);
    }
}
