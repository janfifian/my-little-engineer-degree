﻿using System.Collections;
using System;
using System.Collections.Generic;
using UnityEngine;

public class Waypoint : MonoBehaviour
{

    public float radius;
    public List<Waypoint> Neighbours;

    public Waypoint getNextRandomWaypoint()
    {
        return Neighbours[UnityEngine.Random.Range(0, Neighbours.Count)];
    }

}
