﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TableScript : MonoBehaviour, IDamagable
{
    public int health = 100;

    public void die()
    {
        Destroy(this.gameObject);
    }

    public void recoverHP(int hitpoints)
    {
        health += hitpoints;
    }

    public void takeDamage(int dmg)
    {
        health -= dmg;
        if (health <= 0)
        {
            die();
        }
    }

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
