﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

public class KnifeThrow : ISkill
{
    private Transform knifeprefab;
    private int manaCost;
    private Transform caster;
    private float cdRemaining = 0.0f;
    private float cooldown = 10.0f;

    public KnifeThrow() { 
        this.manaCost = 45;
    }

    public int getManaCost() {
        return manaCost;
    }
    public void setManaCost(int value) {
        this.manaCost = value;
    }

    public void setTarget(Transform target)
    {
        knifeprefab = (Transform)Resources.Load("SkillConstructs/Knife", typeof(Transform));
        caster = target;
    }

    public void execute(int spellpower)
    {
        Transform knife = GameObject.Instantiate(knifeprefab, caster.GetChild(0).position+1.0f*caster.forward, Quaternion.Euler(90, 180-caster.rotation.eulerAngles.y ,90 ));
        knife.GetComponent<Rigidbody>().velocity = caster.forward*(30 + spellpower);
        knife.GetComponent<BulletController>().bulletDamage = 25 + 2 * spellpower;
        if (caster.GetComponent<EnemyAI>() != null)
        {
            knife.GetComponent<BulletController>().enemy = caster.GetComponent<EnemyAI>();
        }
        cdRemaining = cooldown - 0.3f*spellpower;
    }

    public void setCooldown(float cd)
    {
        cooldown = cd;
    }

    public float getCooldownRemaining()
    {
        return cdRemaining;
    }

    public void decreaseCooldown()
    {
        if (cdRemaining > 0)
        {
            cdRemaining -= Time.deltaTime;
        }
    }
}
