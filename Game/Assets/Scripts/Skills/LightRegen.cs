﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LightRegen : ISkill
{
    private int manaCost;
    private Transform caster;
    private float cdRemaining = 0.0f;
    private float cooldown = 10.0f;

    public LightRegen()
    {
        this.manaCost = 50;
    }

    public int getManaCost()
    {
        return manaCost;
    }
    public void setManaCost(int value)
    {
        this.manaCost = value;
    }

    public void setTarget(Transform target)
    {
        caster = target;
    }

    public void execute(int spellpower)
    {
        int ticks = 10 + spellpower / 2;
        int regen = 5 + spellpower / 3;
        cdRemaining = cooldown;
        caster.GetComponent<IBuffable>().regenBoost(ticks, regen);
        GameObject.Instantiate((Transform)Resources.Load("SkillParticleEffects/Light_Regen", typeof(Transform)), caster.GetChild(1));
    }

   

    public void setCooldown(float cd)
    {
        cooldown = cd;
    }

    public float getCooldownRemaining()
    {
        return cdRemaining;
    }

    public void decreaseCooldown()
    {
        if (cdRemaining > 0)
        {
            cdRemaining -= Time.deltaTime;
        }
    }
}
