﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pacify: ISkill
{
    private int manaCost;
    private Transform caster;
    private float cdRemaining = 0.0f;
    private float cooldown = 5.0f;

    public Pacify()
    {
        this.manaCost = 30;
    }

    public int getManaCost()
    {
        return manaCost;
    }
    public void setManaCost(int value)
    {
        this.manaCost = value;
    }

    public void setTarget(Transform target)
    {
        caster = target;
    }

    public void execute(int spellpower)
    {
        cdRemaining = cooldown - 0.3f * spellpower;
        float radius = 15.0f + 2 * spellpower;
        GameObject.Instantiate((Transform)Resources.Load("SkillParticleEffects/Pacify", typeof(Transform)), caster.position, Quaternion.Euler(-90,0,0));
        foreach (GameObject bullet in GameObject.FindGameObjectsWithTag("Nullet"))
        {
            Debug.Log(bullet.name);
            if( Vector3.Distance(bullet.transform.position, caster.position)<=radius){
                GameObject.Destroy(bullet);
            }
        }
    }



    public void setCooldown(float cd)
    {
        cooldown = cd;
    }

    public float getCooldownRemaining()
    {
        return cdRemaining;
    }

    public void decreaseCooldown()
    {
        if (cdRemaining > 0)
        {
            cdRemaining -= Time.deltaTime;
        }
    }
}

