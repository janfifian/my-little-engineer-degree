﻿using UnityEngine;

public interface ISkill
{
     int getManaCost();
     void setManaCost(int value);
    void setCooldown(float cd);
    float getCooldownRemaining();
    void decreaseCooldown();
     void setTarget(Transform target);
     void execute(int spellpower);
}