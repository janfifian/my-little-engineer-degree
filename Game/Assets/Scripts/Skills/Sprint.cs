﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Sprint : ISkill
{
    private int manaCost;
    private Transform caster;
    private float cdRemaining = 0.0f;
    private float cooldown = 10.0f;

    public Sprint()
    {
        this.manaCost = 40;
    }

    public int getManaCost()
    {
        return manaCost;
    }
    public void setManaCost(int value)
    {
        this.manaCost = value;
    }

    public void setTarget(Transform target)
    {
        caster = target;
    }

    public void execute(int spellpower)
    {
        float value = 3.0f + 0.2f * spellpower;
        float duration = 4.0f + 0.4f * spellpower;
        cdRemaining = cooldown;
        caster.GetComponent<IBuffable>().speedBoost(duration, value);
        Transform particles =  GameObject.Instantiate((Transform)Resources.Load("SkillParticleEffects/Sprint", typeof(Transform)), caster.GetChild(1));
        particles.GetComponent<ParticleAutodestruction>().time = duration;
    }



    public void setCooldown(float cd)
    {
        cooldown = cd;
    }

    public float getCooldownRemaining()
    {
        return cdRemaining;
    }

    public void decreaseCooldown()
    {
        if (cdRemaining > 0)
        {
            cdRemaining -= Time.deltaTime;
        }
    }
}
