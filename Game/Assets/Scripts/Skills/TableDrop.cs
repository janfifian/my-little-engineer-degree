﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

public class TableDrop : ISkill
{
    private Transform turretprefab;
    private int manaCost;
    private Transform caster;
    private float cdRemaining = 0.0f;
    private float cooldown = 8.0f;

    public TableDrop() { 
        this.manaCost = 35;
    }

    public int getManaCost() {
        return manaCost;
    }
    public void setManaCost(int value) {
        this.manaCost = value;
    }

    public void setTarget(Transform target)
    {
        turretprefab = (Transform)Resources.Load("SkillConstructs/Table", typeof(Transform));
        caster = target;
    }

    public void execute(int spellpower)
    {
        Transform table = GameObject.Instantiate(turretprefab, caster.position + 2.5f*caster.forward, caster.rotation);
        TableScript tureet = table.GetComponent<TableScript>();
        tureet.health = (60+20 * spellpower);
        cdRemaining = 8.0f - 0.2f*spellpower;
        GameObject.Instantiate((Transform)Resources.Load("SkillParticleEffects/Table_Drop", typeof(Transform)), caster.position + 2.5f * caster.forward,Quaternion.Euler(-90,0,0));
    }

    public void setCooldown(float cd)
    {
        cooldown = cd;
    }

    public float getCooldownRemaining()
    {
        return cdRemaining;
    }

    public void decreaseCooldown()
    {
        if (cdRemaining > 0)
        {
            cdRemaining -= Time.deltaTime;
        }
    }
}
