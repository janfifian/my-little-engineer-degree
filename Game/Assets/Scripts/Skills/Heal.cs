﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Heal : ISkill
{
    private int manaCost;
    private Transform caster;
    private float cdRemaining = 0.0f;
    private float cooldown = 4.0f;

    public Heal()
    {
        this.manaCost = 60;
    }

    public int getManaCost()
    {
        return manaCost;
    }
    public void setManaCost(int value)
    {
        this.manaCost = value;
    }

    public void setTarget(Transform target)
    {
        caster = target;
    }

    public void execute(int spellpower)
    {
        GameObject.Instantiate((Transform)Resources.Load("SkillParticleEffects/Heal", typeof(Transform)), caster);
        int regen = 30 + spellpower *3;
        caster.GetComponent<IDamagable>().recoverHP(regen);
        cdRemaining = cooldown;
    }



    public void setCooldown(float cd)
    {
        cooldown = cd;
    }

    public float getCooldownRemaining()
    {
        return cdRemaining;
    }

    public void decreaseCooldown()
    {
        if (cdRemaining > 0)
        {
            cdRemaining -= Time.deltaTime;
        }
    }
}
