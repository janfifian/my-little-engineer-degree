﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TurretScript : MonoBehaviour, IDamagable {

    public Transform caster;
    public bool castByPlayer;
    public int maxHP = 50;
    public int currentHP;
    public Transform target;
    public Transform firePoint;
    public Transform head;
    public BulletController bullet;
    public int damage = 6;
    public float bulletSpeed = 1.0f;
    private AudioSource audioSource;

    public void die()
    {
        Destroy(this.gameObject);
    }

    public void recoverHP(int hitpoints)
    {
        // Yup, turrets can be healed above their maxHP
        this.currentHP += hitpoints;
    }

    public void shoot()
    {
        if(target.Equals(null))
        {
            retarget();
        }
        head.LookAt(target);
        BulletController newBullet = Instantiate(bullet, new Vector3(firePoint.position.x, 0.5f, firePoint.position.z), head.rotation) as BulletController;
        newBullet.bulletDamage = this.damage;
        newBullet.speed = bulletSpeed;
        newBullet.GetComponent<Rigidbody>().velocity = (newBullet.GetComponent<Transform>().forward).normalized * newBullet.speed;
        audioSource.Play();
    }


    // Use this for initialization
    void Start () {
        this.GetComponent<SphereCollider>().enabled = false;
        currentHP = maxHP;
            head = this.transform.GetChild(2);
            Transform child = head.transform.GetChild(0).transform.GetChild(1);
            if (child.name == "Firepoint")
            {
                this.firePoint = child;
            }
            else
            {
                Debug.Log("Error: Turret firepoint not found");
            }
        audioSource = this.transform.GetComponent<AudioSource>();
        StartCoroutine(InitializeCaster());
    }

    private void decay()
    {
        ((IDamagable)this).takeDamage(1);
    }

    IEnumerator InitializeCaster()
    {
        yield return new WaitForSeconds(0.05f);
        castByPlayer = (caster.GetComponent<PlayerControls>() != null);

        this.GetComponent<SphereCollider>().enabled = true;
        InvokeRepeating("decay", 0.0f, 0.4f);
    }

    void retarget()
    {
        CancelInvoke("shoot");
        if (castByPlayer)
        {
            foreach (EnemyAI enemy in FindObjectsOfType<EnemyAI>())
            {
                if (enemy.currentHP <= 0)
                {
                    CancelInvoke("shoot");
                    continue;
                }
                else
                {
                    if (Vector3.Distance(enemy.transform.position, this.transform.position) < 8.0f)
                    {
                        target = enemy.transform;
                        InvokeRepeating("shoot", 0.1f, 0.7f);
                        break;
                    }
                }
            }
        }
        else
        {
            Transform player = FindObjectOfType<PlayerControls>().transform;
            if (Vector3.Distance(player.transform.position, this.transform.position) < 8.0f)
            {
                target = player.transform;
                InvokeRepeating("shoot", 0.1f, 0.7f);
            }
        }
        if(target == null)
        {
            CancelInvoke("shoot");  
        }
    }

    void IDamagable.takeDamage(int dmg)
    {
        this.currentHP -= dmg;
        if(this.currentHP <= 0)
        {
            die();
        }
    }

    // Update is called once per frame
    void Update () {
	}

    void OnTriggerEnter(Collider col)
    {
        if(castByPlayer && col.GetComponent<EnemyAI>() != null)
        {
            CancelInvoke("shoot");
            target = col.transform;
            InvokeRepeating("shoot", 0.1f, 0.7f);
        }

        if (!castByPlayer && col.GetComponent<PlayerControls>() != null)
        {
            target = col.transform;
            InvokeRepeating("shoot", 0.1f, 0.7f);
        }
    }

    void OnTriggerExit(Collider col)
    {
        if (col.transform.Equals(target))
        {
            CancelInvoke("shoot");
        }
    }



}
