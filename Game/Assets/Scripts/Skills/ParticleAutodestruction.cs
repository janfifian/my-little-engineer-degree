﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ParticleAutodestruction : MonoBehaviour {

    public float time;
	// Use this for initialization
	void Start () {
        StartCoroutine(destroyer());
	}
	
	IEnumerator destroyer()
    {
        yield return new WaitForSeconds(time);
        GameObject.Destroy(this.gameObject);
    }
}
