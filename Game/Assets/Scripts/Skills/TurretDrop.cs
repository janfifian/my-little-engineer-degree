﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

public class TurretDrop : ISkill
{
    private Transform turretprefab;
    private int manaCost;
    private Transform caster;
    private float cdRemaining = 0.0f;
    private float cooldown = 7.0f;

    public TurretDrop() { 
        this.manaCost = 50;
    }

    public int getManaCost() {
        return manaCost;
    }
    public void setManaCost(int value) {
        this.manaCost = value;
    }

    public void setTarget(Transform target)
    {
        turretprefab = (Transform)Resources.Load("SkillConstructs/TurretPrefab", typeof(Transform));
        caster = target;
    }

    public void execute(int spellpower)
    {
        Transform turret = GameObject.Instantiate(turretprefab, caster.position + 3.5f*caster.forward, turretprefab.rotation);
        TurretScript tureet = turret.GetComponent<TurretScript>();
        tureet.caster = caster;
        tureet.damage = 4 + spellpower;
        tureet.maxHP = (35 + 5 * spellpower);
        cdRemaining = cooldown - 0.1f*spellpower;
    }

    public void setCooldown(float cd)
    {
        cooldown = cd;
    }

    public float getCooldownRemaining()
    {
        return cdRemaining;
    }

    public void decreaseCooldown()
    {
        if (cdRemaining > 0)
        {
            cdRemaining -= Time.deltaTime;
        }
    }
}
