﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Blink : ISkill
{
    private Transform target;
    private int manaCost;
    private float cdRemaining = 0.0f;
    private float cooldown = 3.0f;

    public Blink() {
        this.manaCost = 10;
    }

    public int getManaCost() {
        return manaCost;
    }
    public void setManaCost(int value) {
        this.manaCost = value;
    }

    public void setTarget(Transform target)
    {
        this.target = target;
    }

    public void setCooldown(float cd)
    {
        this.cooldown = cd;
    }

    public float getCooldownRemaining()
    {
        return cdRemaining;
    }

    public void decreaseCooldown()
    {
        if (cdRemaining > 0)
        {
            cdRemaining -= Time.deltaTime;
        }
    }

    public void execute(int spellpower)
    {
        Vector3 potentialposition = target.position + target.forward * 7.0f;
        if(Physics.CheckSphere(position: potentialposition,radius: 0.1f,layerMask: Physics.DefaultRaycastLayers , queryTriggerInteraction: QueryTriggerInteraction.Ignore))
        {
        }
        else
        {
            GameObject.Instantiate((Transform)Resources.Load("SkillParticleEffects/Blink", typeof(Transform)),target.position,target.rotation);
            target.position = potentialposition;
            cdRemaining = cooldown * (1.7f - 0.15f * spellpower);
        }
    }
}
