﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using UnityEngine.SceneManagement;

public class PlayerControls : MonoBehaviour, IDamagable, IBuffable
{
    /**
     * Movement speed.
     */
    public float    speed;
    private Rigidbody rb;
    private Vector3 moveInput;
    private Camera mainCamera;
    public IGun gun;

    /**
     *  Defensive parameters
     */
    public int hitPoints = 100;
    private int currentHitPoints;
    public int hpRegen = 1;

    public bool GodMode;

    /**
     *  Attack parameters
     */
    public float bulletSpeedModifier;
    public float damageBoost;
    public float accuracyBoost;
    public float sightRadius;

    /**
     * Experience and shiet.
     */
    private int currentExperience = 0;
    private int level = 0;

    /**
     * UI
     */

    public UISystem UISystem;

    /**
     * Skills and Mana
     */

    public int maxMana;
    private int currentMana;
    public ISkill skill1;
    public int manaRegen = 1;
    public int spellBoost = 0;
    private float skillFullCd; //inversed, as a microoptimisation
    public ISkill Skillpicker()
    {
        string skill  = PlayerPrefs.GetString("skill_choice");

        switch (skill)
        {
            case "Blink":
                UISystem.updateSkillIcon("Blink");
                return new Blink();
            case "Pacify":
                UISystem.updateSkillIcon("Pacify");
                return new Pacify();
            case "Turret":
                UISystem.updateSkillIcon("Turret_Drop");
                return new TurretDrop();
            case "Table Drop":
                UISystem.updateSkillIcon("Table_Drop");
                return new TableDrop();
            case "Mend Wounds":
                UISystem.updateSkillIcon("Heal");
                return new Heal();
            case "Light Regen":
                UISystem.updateSkillIcon("Light_Regen");
                return new LightRegen();
            case "Sprint":
                UISystem.updateSkillIcon("Sprint");
                return new Sprint();
            case "Knife Throw":
                UISystem.updateSkillIcon("Knife_Throw");
                return new KnifeThrow();
            default:
                return null;
        }

    }
    public void AdjustWeapon()
    {
        gun.passStatistics(damageBoost, bulletSpeedModifier, accuracyBoost);
    }

    public Transform Gunpicker()
    {
        string weapon = PlayerPrefs.GetString("weapon_choice");

        switch (weapon)
            {
                case "Pistol":
                    return (Transform)Resources.Load("Guns/Pistol", typeof(Transform));
                case "Shotgun":
                    return (Transform)Resources.Load("Guns/Shotgun", typeof(Transform));
                case "Sniper Rifle":
                    return (Transform)Resources.Load("Guns/Sniper", typeof(Transform));
                case "Assault Rifle":
                    return (Transform)Resources.Load("Guns/Rifle", typeof(Transform));
                case "UZI":
                    return (Transform)Resources.Load("Guns/UZI", typeof(Transform));
                default:
                    return null;
            }
    }

    void Start()
    {
        Transform newGun = Instantiate(Gunpicker(), this.transform);
        this.gameObject.GetComponent<PlayerControls>().gun = newGun.GetComponent<IGun>();
        newGun.GetComponent<IGun>().setFirepoint(this.transform.GetChild(0).transform);
        this.gameObject.GetComponent<PlayerControls>().WeaponSwitchAmmoUpdate();


        skill1 = Skillpicker();

        speed = 5.5f + 0.5f * PlayerPrefs.GetInt("speed_boost");

            {
                int hpstat = PlayerPrefs.GetInt("health_boost");
                if (hpstat <= 5)
                {
                    hitPoints = 70 + hpstat * 10;
                }
                else
                {
                    hitPoints = 120 + (hpstat-5) * 15;
                }
                hpRegen = 1 + ((hpstat>=5) ? 1 : 0) + ((hpstat == 10) ? 1 : 0);
            }

        bulletSpeedModifier = 0.05f + PlayerPrefs.GetInt("bullet_speed_boost") * 0.05f + Mathf.Max(0.0f,(PlayerPrefs.GetInt("bullet_speed_boost") - 5) * 0.1f);

        damageBoost = 0.05f + PlayerPrefs.GetInt("damage_boost") * 0.05f + Mathf.Max(0.0f, (PlayerPrefs.GetInt("damage_boost") - 5) * 0.05f);

        sightRadius = 0.15f + 0.05f* PlayerPrefs.GetInt("sight_radius") + Mathf.Max(0.0f, (PlayerPrefs.GetInt("sight_radius") - 5) * 0.012f);

        accuracyBoost = 1.0f - 0.05f * PlayerPrefs.GetInt("accuracy_boost");
        if(accuracyBoost <= 0.75f){
            accuracyBoost -= 0.05f * (PlayerPrefs.GetInt("accuracy_boost") - 5);
        }
            {
                spellBoost = PlayerPrefs.GetInt("mana_boost");
                if (spellBoost <= 5)
                {
                    maxMana = 70 + spellBoost * 10;
                }
                else
                {
                    maxMana = 120 + (spellBoost - 5) * 15;
                }
                manaRegen = 1 + ((spellBoost >= 5) ? 1 : 0) + ((spellBoost == 10) ? 1 : 0);
            }
        /*
         * Just to be sure whats in player prefs
         
        Debug.Log("Displaying Stored Data: \n BSPD: " +
        PlayerPrefs.GetInt("bullet_speed_boost") + "\n DMG: " + 
        PlayerPrefs.GetInt("damage_boost") + "\n SGHT: " +
        PlayerPrefs.GetInt("sight_radius") + "\n ACC: " +
        PlayerPrefs.GetInt("accuracy_boost") + "\n HP: " +
        PlayerPrefs.GetInt("health_boost") + "\n MP: " +
        PlayerPrefs.GetInt("mana_boost") + "\n SPD: " +
        PlayerPrefs.GetInt("speed_boost")
        );

        /**
         * End;
         */

        if (GodMode)
        {
            damageBoost = 30;
            hpRegen = hitPoints;
            spellBoost = 10;
            manaRegen = maxMana;
        }
        level = 1;
        currentExperience = 0;
        rb = GetComponent<Rigidbody>();
        mainCamera = FindObjectOfType<Camera>();
        currentHitPoints = hitPoints;
        currentMana = maxMana;
        InvokeRepeating("regenerateHP", 1.0f, 1.0f);
        InvokeRepeating("regenerateMP", 1.0f, 1.0f);
        StartCoroutine(InitializeUI());
        gun = this.transform.GetChild(this.transform.childCount-1).GetComponent<IGun>();
        AdjustWeapon();
        skill1.setTarget(this.GetComponent<Transform>());
    }


    void Update()
        {
        moveInput = new Vector3(Input.GetAxis("Horizontal"), 0, Input.GetAxis("Vertical"));
        moveInput = moveInput.normalized * speed;

            if (Input.GetKeyDown(KeyCode.Q))
            {
                if (currentMana >= skill1.getManaCost() && skill1.getCooldownRemaining()<=0.0f)
                {
                    currentMana -= skill1.getManaCost();
                    skill1.execute(spellBoost);
                    skillFullCd = 1.0f/skill1.getCooldownRemaining();
                    MPUpdate();
                }
            }

        /**
         * Movement and rotation.
         */
        Ray cameraRay = mainCamera.ScreenPointToRay(Input.mousePosition);
        Plane groundPlane = new Plane(Vector3.up, new Vector3 (0,1f, 0));
        float rayLength;
        if(groundPlane.Raycast(cameraRay, out rayLength))
        {
            Vector3 crosshairs = cameraRay.GetPoint(rayLength);
            transform.LookAt(new Vector3(crosshairs.x, this.transform.position.y, crosshairs.z));
        }
        rb.MovePosition(rb.position + moveInput * Time.fixedDeltaTime);

        /**
         * Shooting
         */
        if (Input.GetMouseButton(0) && gun.isReady())
        {
            gun.shoot();
            AmmoUpdate();
        }

        if (Input.GetKeyDown(KeyCode.R) && gun.isReady())
        {
            StartCoroutine(reloadUpdateUI());
        }

        if (gun.getDelay() >= 0.0f)
        {
            gun.reduceDelay();
        }
        if (skill1.getCooldownRemaining() >= 0.0f)
        {
            skill1.decreaseCooldown();
        }
        UISystem.updateSkillCd(1-skill1.getCooldownRemaining() * skillFullCd);
    }

    private IEnumerator reloadUpdateUI()
    {
        gun.reload();
        yield return new WaitForSeconds(gun.getReloadTime());
        AmmoUpdate();
    }

    /**
     * Defensive parts - taking damage and dying
     */

    public void takeDamage(int dmg)
    {
        currentHitPoints -= dmg;
        HPUpdate();
        if(currentHitPoints <= 0)
        {
            die();
        }
    }

    public void speedBoost(float duration, float value)
    {
        StartCoroutine(SpeedBoost(duration, value));
    }

    IEnumerator SpeedBoost(float duration, float value)
    {
        speed += value;
        yield return new WaitForSeconds(duration);
        speed -= value;
    }

    public void regenBoost(int ticks, int value)
    {
        StartCoroutine(LightHealing(ticks, value));
    }

    IEnumerator LightHealing(int ticks, int regen)
    {
        for (int i = 0; i < ticks; i++)
        {
            recoverHP(regen);
            yield return new WaitForSeconds(10.0f / ticks);
        }
    }

    private void regenerateMP()
    {
        currentMana = Mathf.Min(maxMana, currentMana + manaRegen);
        MPUpdate();
    }

    private void regenerateHP()
    {
        currentHitPoints = Mathf.Min(hitPoints, currentHitPoints + hpRegen);
        HPUpdate();
    }

    public void recoverHP(int hitpoints)
    {
        currentHitPoints = Mathf.Min(hitPoints, currentHitPoints + hitpoints);
        HPUpdate();
    }

    public void die()
    {
        PlayerPrefs.SetInt("total_score",UISystem.score);
        FindObjectOfType<EnemyGeneticSystem>().GetComponent<EnemyGeneticSystem>().saveLastEnemyConfigs();
        SceneManager.LoadScene(5);
    }



    /**
     * Experience and stats -- probably not gonna use it till further development.
     * This one is just for the future.
     */

    public void acquireExperience(int expGained)
    {
        this.currentExperience += expGained;
        int expForNextLevel = (100 + level * 10);
        expForNextLevel *= expForNextLevel;
        if (currentExperience >expForNextLevel)
        {
            currentExperience -= expForNextLevel;
            level++;
        }
    }

    /**
     * Updating UI
     */

    private void HPUpdate()
    {
        UISystem.UpdateUI(0, "<color=\"red\"><align=\"right\">" + currentHitPoints + "/" + hitPoints + "</align>");
    }

    private void MPUpdate()
    {
        UISystem.UpdateUI(1, "<color=\"blue\"><align=\"right\">" + currentMana + "/" + maxMana + "</align>");
    }

    private void AmmoUpdate()
    {
        UISystem.UpdateUI(3, "<align=\"left\">" + gun.getAmmo() + "</align>");
    }

    public void WeaponSwitchAmmoUpdate()
    {
        UISystem.updateAmmoIcon(gun.getName());
        AmmoUpdate();
    }



    /**
     * Initialize UI.
     */
    IEnumerator InitializeUI()
    {
        yield return new WaitForSeconds(Time.deltaTime);
        WeaponSwitchAmmoUpdate();
        HPUpdate();
        MPUpdate();
        
    }
}
