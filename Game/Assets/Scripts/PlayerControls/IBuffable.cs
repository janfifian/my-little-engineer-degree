﻿public interface IBuffable
{
    void regenBoost(int ticks, int value);
    void speedBoost(float duration, float value);
}