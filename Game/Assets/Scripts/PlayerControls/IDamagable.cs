﻿public interface IDamagable
{
    void takeDamage(int dmg);
    void recoverHP(int hitpoints);
    void die();
}