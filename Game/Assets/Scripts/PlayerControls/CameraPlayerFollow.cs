﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Rendering.PostProcessing;

public class CameraPlayerFollow : MonoBehaviour {

    public Transform player;
	// Use this for initialization
	void Start () {
        StartCoroutine(RuntimeInitialize());
    }

    private IEnumerator RuntimeInitialize()
    {
        yield return new WaitForSeconds(0.02f);
        Vignette vig;
        PostProcessVolume volume = this.GetComponent<PostProcessVolume>();
        volume.profile.TryGetSettings<Vignette>(out vig);
        vig.intensity.value = 1.0f - player.GetComponent<PlayerControls>().sightRadius;
    }
	
	// Update is called once per frame
	void Update () {
        this.transform.position = new Vector3(player.position.x, 20, player.position.z - 5);
    }
}
