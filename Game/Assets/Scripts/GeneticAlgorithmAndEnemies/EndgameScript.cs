﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class EndgameScript : MonoBehaviour {

    public TextMeshProUGUI text;


    void Awake()
    {
        int score = PlayerPrefs.HasKey("total_score") ? PlayerPrefs.GetInt("total_score") : 0;
        //   Debug.Log("11<size=200%>You've lost!<size=150%>\n\n Your score: " + score.ToString() + "\n\n <size = 100 %> Enemy generations defeated:" + (score / 9).ToString());
        text.text = "<size=200%>You've lost!<size=150%>\n\n Your score: " + score.ToString() + "\n\n <size=100%> Enemy generations defeated:" + (score / 9).ToString();
    }

    // Update is called once per frame
    void Update () {
		
	}
}
