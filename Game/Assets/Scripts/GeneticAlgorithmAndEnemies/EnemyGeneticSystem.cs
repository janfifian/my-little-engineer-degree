﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

public class EnemyGeneticSystem : MonoBehaviour {
    /*
     * Saves enemy configuration from current population
     */
    void saveTextFileAsEnemyConfigs()
    {
        string path = "Public\\TowerOfGenesisOutput";
        if (Directory.Exists(path))
        {
        }
        else
        {
             Directory.CreateDirectory(path);

        }

        DateTime localDate = DateTime.Now;
        string outputFileName = "Population_generated_at_"+localDate.ToLongDateString()+ UnityEngine.Random.Range(0.0f,100.0f).ToString()+ ".txt";
        using (StreamWriter outputFile = new StreamWriter(Path.Combine(path, outputFileName)))
        {
            for(int i = 0; i < 9; i++)
            {
                string Line = "";
                for(int j = 0; j < 7; j++)
                {
                    Line += stats[i][j].ToString() + '\t';
                }
                Line += weapons[i].ToString() + '\t' + skills[i].ToString() + '\t' + scores[i].ToString();
                Debug.Log(Line);
                outputFile.WriteLine(Line);
            }
        }
        Debug.Log("Succesfully saved at " + Path.Combine(path, outputFileName));

    }



    void readTextFileAsEnemyConfigs(string file_path)
    {
        using (TextReader reader = File.OpenText(file_path))
        {
            for(int j = 0; j < 9; j++)
            {
                string enemyDescription = reader.ReadLine();
                string[] enemyDescriptionPieces = enemyDescription.Split('\t');
                List<int> unit = new List<int> { int.Parse(enemyDescriptionPieces[0]),
                                                 int.Parse(enemyDescriptionPieces[1]),
                                                 int.Parse(enemyDescriptionPieces[2]),
                                                 int.Parse(enemyDescriptionPieces[3]),
                                                 int.Parse(enemyDescriptionPieces[4]),
                                                 int.Parse(enemyDescriptionPieces[5]),
                                                 int.Parse(enemyDescriptionPieces[6]),};
                stats.Add(new List<int>());
                stats[j] = unit;
                weapons[j] = int.Parse(enemyDescriptionPieces[7]);
                skills[j] = int.Parse(enemyDescriptionPieces[8]);
            }
        }
    
       
    }

    public void saveLastEnemyConfigs()
    {
        string path = "Public\\TowerOfGenesisOutput";
        if (Directory.Exists(path))
        {
        }
        else
        {
            Directory.CreateDirectory(path);

        }

        DateTime localDate = DateTime.Now;
        string outputFileName = "LastEnemyGeneration.txt";
        using (StreamWriter outputFile = new StreamWriter(Path.Combine(path, outputFileName)))
        {
            for (int i = 0; i < 9; i++)
            {
                string Line = "";
                for (int j = 0; j < 7; j++)
                {
                    Line += stats[i][j].ToString() + '\t';
                }
                Line += weapons[i].ToString() + '\t' + skills[i].ToString() + '\t' + scores[i].ToString();
                Debug.Log(Line);
                outputFile.WriteLine(Line);
            }
        }
        Debug.Log("Succesfully saved at " + Path.Combine(path, outputFileName));
                         }

    string getGun(int i)
    {
        switch (i)
        {
            case 1:
                return "Pistol";
            case 2:
                return "Shotgun";
            case 3:
                return "Sniper Rifle";
            case 4:
                return "Assault Rifle";
            case 5:
                return "UZI";
            default:
                return "";
        }
    } 

    public int enemies_killed;
    public List<Transform> grid = new List<Transform>();
    public UISystem uisystem;
    public bool ezMode;
    public float[] scores ={0,0,0,0,0,0,0,0,0};
    public List<List<int>> stats = new List<List<int>>();
    public int[] weapons = { 0, 0, 0, 0, 0, 0, 0, 0, 0 };
    public int[] skills = { 0, 0, 0, 0, 0, 0, 0, 0, 0 };
    public Transform EnemyPrefab;
    // Use this for initialization
    void Start () {
        //Added spawnpoints;
        GameObject go = new GameObject();
        for (int i = 0; i < 9; i++)
        {

            grid.Add(Instantiate(go, this.transform.GetChild(0)).transform);
        }
        Destroy(go);

        List<Vector3> positions = new List<Vector3>();
        positions.Add(new Vector3(-37, 1, -10));
        positions.Add(new Vector3(-31, 1, -40));
        positions.Add(new Vector3(-4, 1, -44));
        positions.Add(new Vector3(16.5f, 1, -43));
        positions.Add(new Vector3(45, 1, 45));
        positions.Add(new Vector3(12, 1, 46));
        positions.Add(new Vector3(43, 1, -45));
        positions.Add(new Vector3(-46, 1, 6));
        positions.Add(new Vector3(-46, 1, 36));

        for(int i = 0; i < 9; i++)
        {
            grid[i].position = positions[i];
            
        }
        ezMode = PlayerPrefs.GetInt("ez_mode")>0;
        enemies_killed = 0;
        StartCoroutine(InitializeUI());
        spawnFirstGeneration();
    }

    IEnumerator InitializeUI()
    {
        yield return new WaitForSeconds(Time.deltaTime);
        UpdateUI();
    }

	// Call it whenever something happens;
	void UpdateUI () {
        uisystem.UpdateUI(2, "<align=\"left\">Score:</align> " + enemies_killed);
        uisystem.score = enemies_killed;
    }

    public void incrementScore()
    {
        enemies_killed++;
        UpdateUI();
        if (enemies_killed % 9 == 0)
        {
            spawnNewGeneration();
        }
    }

    IEnumerator setStats(EnemyAI ai, int i)
    {
        yield return new WaitForSeconds(0.02f);
        ai.enemyIndex = i;
        ai.initializeStats(stats[i], ezMode);
        ai.InitializeGun(getGun(weapons[i]));
        ai.initializeSkill(skills[i]);
    }

    private void spawnFirstGeneration()
    {
        int gen_mode = PlayerPrefs.GetInt("enemies_generation_mode");

        Debug.Log("Generating enemies with mode " + gen_mode);
        switch (gen_mode)
        {
            case 0:
                for (int i = 0; i < 9; i++)
                {
                    stats.Add(new List<int>());
                    createRandomUnit(i);
                }
                break;
            case 1:
                readTextFileAsEnemyConfigs("Public/TowerOfGenesisOutput/LastEnemyGeneration.txt");
                break;
            case 2:
                readTextFileAsEnemyConfigs("Assets/Resources/TextResources/PredefinedEnemies.txt");
                break;
          
        }

        for (int i = 0; i < 9; i++)
        {
            Transform go = Instantiate(EnemyPrefab, this.transform);
            go.position = grid[i].position;
            StartCoroutine(setStats(go.GetComponent<EnemyAI>(), i));
        }
    }

    private void increaseRandom(List<int> stat)
    {
        int slot;
        while (true)
        {
            slot= UnityEngine.Random.Range(0, 7);
            if(stat[slot] < 10)
            {
                stat[slot]++;
                return;
            }
        }
    }

    private void decreaseRandom(List<int> stat)
    {
        int slot;
        while (true)
        {
            slot = UnityEngine.Random.Range(0, 7);
            if (stat[slot] > 0)
            {
                stat[slot]--;
                return;
            }
        }
    }

    void createRandomUnit (int j)
    {
        List<int> unit = new List<int>{0,0,0,0,0,0,0};
        for (int iterator = 0; iterator<35 ; iterator++)
        {
            increaseRandom(unit);
        }
        stats[j] = unit;
        weapons[j] = UnityEngine.Random.Range(1,6);
        skills[j] = UnityEngine.Random.Range(1, 9);
    }

    private void mutate(List<int> stat)
    {
        increaseRandom(stat);
        decreaseRandom(stat);
    }

    private void mutateWeaponsAndSkills(int j)
    {
        if (UnityEngine.Random.Range(0.0f, 1.0f) > 0.75f)
        {
            weapons[j] = UnityEngine.Random.Range(1, 6);
        }
        if (UnityEngine.Random.Range(0.0f, 1.0f) > 0.75f)
        {
            skills[j] = UnityEngine.Random.Range(1, 8);
        }
    }

    private List<int> crossOver(int slot1, int slot2)
    {
        List<int> child = new List<int>();
        int accumulated = 0;
        for(int i = 0; i < 7; i++)
        {
            child.Add(stats[slot1][i] + stats[slot2][i]);
            if (child[i] % 2 == 0)
            {
                child[i] /= 2;
            }
            else
            {
                child[i] /= 2;
                if (stats[slot1][i] > stats[slot2][i])
                {
                    child[i]++;
                }
            }
            accumulated += child[i];
        }
        while (accumulated > 35)
        {
            decreaseRandom(child);
            accumulated--;
        }
        while (accumulated < 35)
        {
            increaseRandom(child);
            accumulated++;
        }
        return child;
    }

    private List<int> bestIndices()
    {
        List<int> best = new List<int> { -1, -1, -1 };

        float third=-1;
        float first=-1;
        float second=-1;
        for (int i = 0; i < 9; i++)
        {
            // If current element is  
            // greater than first 
            if (scores[i] >= first)
            {
                third = second;
                second = first;
                first = scores[i];
                best[2] = best[1];
                best[1] = best[0];
                best[0] = i;
            }
            else if (scores[i] >= second)
            {
                third = second;
                second = scores[i];
                best[2] = best[1];
                best[1] = i;
            }
            else if (scores[i] >= third)
            {
                best[2] = i;
                third = scores[i];
            }
        }
        return best;
    }

    int randomSelect(int i, int j, float probI,float probJ, int rangeMin, int rangeMax, int[] sourcetable)
    {
        float randNumb = (UnityEngine.Random.Range(0.0f, 1.0f));
        if ( randNumb < probI)
        {
            return sourcetable[i];
        }
        if (randNumb < probI + probJ)
        {
            return sourcetable[j];
        }

        return UnityEngine.Random.Range(rangeMin, rangeMax);
    }

    private void spawnNewGeneration()
    {
        saveTextFileAsEnemyConfigs();
        List<int> bestInd = bestIndices();
        int[] oldWeapons = (int[])weapons.Clone();
        int[] oldSkills = (int[])skills.Clone();
        List<List<int>> newStats = new List<List<int>>();
        for (int i = 0; i < 3; i++)
        {
            for (int j = i; j < 3; j++)
            {
                newStats.Add(crossOver(bestInd[i], bestInd[j]));
                weapons[newStats.Count - 1] = randomSelect(bestInd[i], bestInd[j], 0.6f, 0.3f, 1, 6, oldWeapons);
                skills[newStats.Count - 1] = randomSelect(bestInd[i], bestInd[j], 0.6f, 0.3f, 1, 8, oldSkills);
            }
        }
        newStats.Add(stats[bestInd[0]]);
        newStats.Add(stats[bestInd[1]]);
        mutateWeaponsAndSkills(6);
        mutateWeaponsAndSkills(7);
        for (int i = 1; i < 7; i++)
        {
            mutate(newStats[i]);
        }
        stats = newStats;
        stats.Add(stats[0]);
        createRandomUnit(8);




        for (int i = 0; i < 9; i++)
        {
            Transform go =Instantiate(EnemyPrefab,this.transform);
            go.GetComponent<EnemyAI>().enemyIndex = i;
            go.position = grid[i].position;
            StartCoroutine(setStats(go.GetComponent<EnemyAI>(), i));
        }
    }
}
