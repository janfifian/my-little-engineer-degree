﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.Audio;
using UnityEngine;
using UnityEditor;



public class EnemyAI : MonoBehaviour, IDamagable, IBuffable
{

    public enum Stance { FightHealthy, FightWounded, Patrol, Dying };
    public Stance currentStance;

    public int enemyIndex;

    /*Player Detecting*/

    private const float SHOTGUNDIST = 4.0f;
    private const float PISTOLDIST = 7.0f;
    private const float SNIPERDIST = 10.0f;
    private const float UZIDIST = 5.5f;
    private const float CARABINEDIST = 8.0f;

    private float safeDistance;
    private float shootingDistance;
    private Transform playerAvatar;

    public float score = 0.0f;

    private float runningTime = 0.0f; //time between picking next random point to move towards in case of fight


    /* DISTS are for approaching and keeping distance in case of particular weapon. */

    public float sightRadius = 10.0f;
    public float speed = 4.0f;
    public int maxHP;
    public float bulletSpeedModifier;
    public float damageBoost;
    public float accuracyBoost;
    public int spellBoost;

    private int hpRegen;
    private int maxMana;
    private int currentMana;
    public ISkill skill1;
    private bool skillTypeOffensive = false;
    private int manaRegen = 1;

    private Waypoint currentwaypoint;
    private Vector3 target;

    private Rigidbody rb;
    public IGun gun;
    public string weapon = "Pistol";


    public int currentHP;
    private AudioSource audiosource;



    public void initializeStats(List<int> stats, bool ezMode)
    {
        sightRadius = 4.5f+0.6f*stats[0];
        this.gameObject.GetComponent<SphereCollider>().radius = sightRadius;
        speed = 5.5f + 0.5f *stats[1];
        if (stats[2] <= 5)
        {
            maxHP = 70 + stats[2] * 10;
        }
        else
        {
            maxHP = 120 + (stats[2] - 5) * 15;
        }
        if (ezMode)
        {
            maxHP =maxHP*3/4;
        }
        currentHP = maxHP; hpRegen = 1 + ((stats[2] >= 5) ? 1 : 0) + ((stats[2] == 10) ? 1 : 0);
        bulletSpeedModifier = 0.05f + stats[3] * 0.05f + Mathf.Max(0.0f, (stats[3] - 5) * 0.1f);
        damageBoost = 0.05f + stats[4] * 0.05f + Mathf.Max(0.0f, (stats[4] - 5) * 0.05f);
        if (ezMode)
        {
            damageBoost -=0.5f;
        }
        accuracyBoost = 1.0f - 0.05f * stats[5];
        if (accuracyBoost <= 0.75f)
        {
            accuracyBoost -= 0.05f * (stats[5] - 5);
        }
        spellBoost = stats[6];
        if (spellBoost <= 5)
        {
            maxMana = 70 + spellBoost * 10;
        }
        else
        {
            maxMana = 120 + (spellBoost - 5) * 15;
        }
        manaRegen = 1 + ((spellBoost >= 5) ? 1 : 0) + ((spellBoost == 10) ? 1 : 0);
        currentMana = maxMana;

        CancelInvoke("regenHP");
        CancelInvoke("regenMana");
        InvokeRepeating("regenHP", 1.0f, 1.0f);
        InvokeRepeating("regenMana", 1.0f, 1.0f);
    }

    void regenHP()
    {
        currentHP = Mathf.Min(maxHP, currentHP + hpRegen);
    }

    void regenMana()
    {
        currentMana = Mathf.Min(maxMana, currentMana + manaRegen);
    }

    ISkill getSkill(int id)
    {
        switch (id)
        {
            case 1:
                return new Blink();
            case 2:
                return new Pacify();
            case 3:
                return new LightRegen();
            case 4:
                return new TableDrop();
            case 5:
                return new Heal();
            case 6:
                return new Sprint();
            case 7:
                return new TurretDrop();
            case 8:
                return new KnifeThrow();
            default:
                if (skill1.Equals(null))
                {
                    Debug.Log(id+ " Failed to initialize.");
                }
                return null;
        }
    }

    public void initializeSkill(int id)
    {
        this.skill1 = getSkill(id);
        this.skillTypeOffensive = (id > 5);
        skill1.setTarget(this.GetComponent<Transform>());
    }

    public void InitializeGun(string wpn)
    {
        weapon = wpn;
        //initializeWeapon
        Transform newGun;
        switch (weapon)
        {
            case "Pistol":
                newGun = Instantiate((Transform)Resources.Load("Guns/Pistol", typeof(Transform)), this.transform);
                safeDistance = 0.5f * PISTOLDIST;
                shootingDistance = PISTOLDIST;
                break;
            case "Shotgun":
                newGun = Instantiate((Transform)Resources.Load("Guns/Shotgun", typeof(Transform)), this.transform);
                safeDistance = 0.5f * SHOTGUNDIST;
                shootingDistance = SHOTGUNDIST;
                break;
            case "Sniper Rifle":
                newGun = Instantiate((Transform)Resources.Load("Guns/Sniper", typeof(Transform)), this.transform);
                safeDistance = 0.5f * SNIPERDIST;
                shootingDistance = SNIPERDIST;
                break;
            case "Assault Rifle":
                newGun = Instantiate((Transform)Resources.Load("Guns/Rifle", typeof(Transform)), this.transform);
                safeDistance = 0.5f * CARABINEDIST;
                shootingDistance = CARABINEDIST;
                break;
            case "UZI":
                newGun = Instantiate((Transform)Resources.Load("Guns/UZI", typeof(Transform)), this.transform);
                safeDistance = 0.5f * UZIDIST;
                shootingDistance = UZIDIST;
                break;
            default:
                newGun = null; break;
        }
        this.gameObject.GetComponent<EnemyAI>().gun = newGun.GetComponent<IGun>();
        newGun.GetComponent<IGun>().setFirepoint(this.transform.GetChild(0).transform);
        gun.passStatistics(damageBoost, bulletSpeedModifier, accuracyBoost);
        gun.setHolder(this.transform);
    }

    void OnTriggerEnter(Collider col)
    {
        if(col.gameObject.name == "Player")
        {
            playerAvatar = col.transform;
            currentStance = currentHP>maxHP/2? Stance.FightHealthy:Stance.FightWounded;
        }
    }

    public void die()
    {
        GameObject.Instantiate((Transform)Resources.Load("SkillConstructs/DyingBreath", typeof(Transform)),this.transform.position,this.transform.rotation);
        foreach (Transform child in transform)
        {
            GameObject.Destroy(child.gameObject);
        }
        FindObjectOfType<EnemyGeneticSystem>().GetComponent<EnemyGeneticSystem>().scores[enemyIndex] = score;
        Destroy(this.transform.gameObject);
        Destroy(this.gameObject.GetComponent<Renderer>());
        Destroy(this.gameObject.GetComponent<CapsuleCollider>());
        Destroy(this.gameObject.GetComponent<CapsuleCollider>());
        Destroy(this.gameObject.GetComponent<MeshFilter>());
        StartCoroutine(death());
    }

    IEnumerator death()
    {
        audiosource.Play();
        yield return new WaitForSeconds(audiosource.clip.length);
        Destroy(this.transform.gameObject);
    }



    public void recoverHP(int hitpoints)
    {
        currentHP = Mathf.Min(maxHP, currentHP + hitpoints);
    }

    public void takeDamage(int dmg)
    {
        if (currentHP > 0)
        {
            currentHP -= dmg;
        }
        if(currentHP <= 0)
        {
            if (currentStance != Stance.Dying)
            {
                this.transform.parent.GetComponent<EnemyGeneticSystem>().incrementScore();
                this.currentStance = Stance.Dying;
            }
            die();
        }
    }

    private void combatMove()
    {
        float step = speed * Time.deltaTime;
        if(playerAvatar == null)
        {
           playerAvatar = FindObjectOfType<PlayerControls>().transform;
        }
        float d = Vector3.Distance(playerAvatar.position, this.transform.position);
        if (d < safeDistance)
        {
            // calculate distance to move
            transform.position = Vector3.MoveTowards(transform.position, playerAvatar.position, -step);
        }
        else if (d > shootingDistance)
        {
            transform.position = Vector3.MoveTowards(transform.position, playerAvatar.position, step);
        }
        else
        {
            runningTime -= Time.deltaTime;
            transform.position = Vector3.MoveTowards(transform.position, target, step);
            if (runningTime < 0.0f)
            {
                dodge();
                runningTime = 0.3f;
            }
        }
        score += 5*Time.deltaTime;
    }

    // Use this for initialization
    void Start () {
        audiosource = this.GetComponent<AudioSource>();
        string path = "Sounds/DeathSounds/" + Random.Range(1, 5);
        audiosource.clip = (AudioClip)Resources.Load(path, typeof(AudioClip));      
        rb = this.GetComponent<Rigidbody>();
        getCurrentWaypoint();
        currentStance = Stance.Patrol;
    }

    private void getCurrentWaypoint()
    {
        float closest = 1000.0f;
        foreach(Transform wtarget in transform.parent.GetChild(1))
        {
            float d = Vector3.Distance(wtarget.transform.position, this.transform.position);
            if (closest >d )
            {
                this.currentwaypoint = wtarget.GetComponent<Waypoint>();
                closest = d;
            }
        }

        target = currentwaypoint.transform.position;
    }

    private void nextTarget()
    {
        currentwaypoint = this.currentwaypoint.getNextRandomWaypoint();
        this.target = currentwaypoint.transform.position + UnityEngine.Random.insideUnitSphere*currentwaypoint.radius;
        target.y = 1.0f;
    }

    private void nextRetarget()
    {
        this.target = currentwaypoint.transform.position + UnityEngine.Random.insideUnitSphere * currentwaypoint.radius;
        target.y = 1.0f;
    }

    private void dodge()
    {
        this.target = this.transform.position + UnityEngine.Random.insideUnitSphere * 2.0f;
        target.y = 1.0f;
    }

    private void tryShooting()
    {
        this.transform.LookAt(playerAvatar);
        if (gun.getDelay() <= 0.0f && gun.isReady())
        {
            if (!gun.gottaReload())
            {
                gun.reload();
            }
            else
            {
                gun.shoot();
            }
        }
        else
        {
            gun.reduceDelay();
        }
    }

    private void useSkill()
    {
        if (currentMana >= skill1.getManaCost() && skill1.getCooldownRemaining() <= 0.0f)
        {
            currentMana -= skill1.getManaCost();
            skill1.execute(spellBoost);
        }
    }

    private void CombatStances()
    {
        combatMove();
        /**ENOUGH WITH MOVING, SHOOTING TIME**/
        tryShooting();
        if (skillTypeOffensive)
        {
            useSkill();
        }
        
        if (Vector3.Distance(this.transform.position, playerAvatar.transform.position) > 2.0f * sightRadius)
        {
            nextRetarget();
            currentStance = Stance.Patrol;
        }
        transform.position = new Vector3(transform.position.x, 1, transform.position.z);
    }

    // Update is called once per frame
    void Update ()
    {

        if (skill1.getCooldownRemaining() >= 0.0f)
        {
            skill1.decreaseCooldown();
        }
        switch (currentStance)
        {
            case Stance.Patrol:
                if (target.Equals(null)) { nextTarget(); }
                if (Vector3.Distance(target,this.transform.position)<0.1f) {
                    nextTarget();
                    transform.LookAt(target);
                }
                else
                {
                    transform.position = Vector3.MoveTowards(transform.position, target, speed * Time.deltaTime);
                }
                break;
            case Stance.FightHealthy:
                combatMove();
                /**ENOUGH WITH MOVING, SHOOTING TIME**/
                tryShooting();
                if (skillTypeOffensive)
                {
                    if (skill1.GetType() == typeof(TurretDrop) && skill1.getCooldownRemaining() <= 0)
                    {
                        dodge();
                        this.transform.LookAt(target);
                    }
                    useSkill();
                }
                if (Vector3.Distance(this.transform.position, playerAvatar.transform.position)>2.0f*sightRadius)
                { nextRetarget();
                    currentStance = Stance.Patrol;
                 }
                transform.position = new Vector3(transform.position.x, 1, transform.position.z);
                break;
            case Stance.FightWounded:
                CombatStances();
                    if (skill1.GetType() == typeof(Blink) && skill1.getCooldownRemaining()<=0)
                    {
                        dodge();
                        this.transform.LookAt(target);
                    }
                    useSkill();
                break;
            case Stance.Dying:
                break;
        }
        
    }

    IEnumerator LightHealing(int ticks, int regen)
    {
        for (int i = 0; i < ticks; i++)
        {
            recoverHP(regen);
            yield return new WaitForSeconds(10.0f / ticks);
        }
    }

    public void regenBoost(int ticks, int value)
    {
        StartCoroutine(LightHealing(ticks, value));
    }

    IEnumerator SpeedBoost(float duration, float value)
    {
        speed += value;
        yield return new WaitForSeconds(duration);
        speed -= value;
    }

    public void speedBoost(float duration, float value)
    {
        StartCoroutine(SpeedBoost(duration, value));
    }
}
