﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletController : MonoBehaviour {

    public float speed;
    public int bulletDamage;
    public EnemyAI enemy;

	void Start () {
        
    }
	
	// Update is called once per frame

    private void OnCollisionEnter(Collision collision)
    {
        IDamagable target = collision.gameObject.GetComponent<IDamagable>();
        if(target != null)
        {
            target.takeDamage(bulletDamage);
            if (collision.gameObject.name == "Player")
            {
                if (enemy!=null)
                {
                    enemy.score += (bulletDamage * 0.5f);
                }
            }
            Destroy(this.gameObject);
        }
        if (collision.gameObject.CompareTag("Wall"))
        {
            Destroy(this.gameObject);
        }
    }
}
