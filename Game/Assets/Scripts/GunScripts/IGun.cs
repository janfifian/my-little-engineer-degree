﻿using UnityEngine;

public interface IGun
{
    void setHolder(Transform holder);
    void reload();
    void shoot();
    string getAmmo();
    bool isReady();
    float getDelay();
    void reduceDelay();
    string getName();
    void setFirepoint(Transform firepoint);
    float getReloadTime();
    bool gottaReload();
    void passStatistics(float bulletDamageBoost, float bulletSpeedBoost, float accuracyBoost);
}