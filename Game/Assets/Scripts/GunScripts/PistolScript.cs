﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;
using UnityEditor;

public class PistolScript : MonoBehaviour, IGun {
    EnemyAI enemy;
    public int currentMagazineSize;
    public int magazineSize = 7;
    public BulletController bullet;
    public Transform firePoint;
    public float bulletSpeed = 30.0f;
    public float reloadSpeed = 1.0f;
    private float shootDelay = 0.0f;
    public int bulletDamage = 15;
    public float shotsPerSecond = 2.0f;
    public float spread = 0.2f;
    private bool readyToShoot = true;
    private AudioSource soundmaker;
    private AudioClip reloadSound;

    public void setHolder(Transform holder)
    {
        enemy = holder.GetComponent<EnemyAI>();
    }

    public bool gottaReload()
    {
        return currentMagazineSize > 0;
    }
    public bool isReady()
    {
        return readyToShoot;
    }

    public float getDelay()
    {
        return shootDelay;
    }

    public string getName()
    {
        return "pistol";
    }

    public float getReloadTime()
    {
        return reloadSpeed;
    }

    public void setFirepoint(Transform firepoint)
    {
        this.firePoint = firepoint;
    }

    public void reduceDelay()
    {
        shootDelay -= Time.deltaTime;
    }

    public void passStatistics(float bulletDamageBoost, float bulletSpeedBoost, float accuracyBoost)
    {
        this.bulletDamage = (int)Math.Round(bulletDamage* (1+bulletDamageBoost));
        this.bulletSpeed *= (1 + bulletSpeedBoost);
        this.spread *= accuracyBoost;
    }

    public void reload()
    {
        if (currentMagazineSize < magazineSize)
        {
            soundmaker.clip = reloadSound;
            soundmaker.Play();
            StartCoroutine(reloading());
        }
    }

    public void shoot()
    {
        if (currentMagazineSize > 0 && shootDelay<=0.0f)
        {
            shootDelay = 1.0f / shotsPerSecond;
            if(firePoint == null) { return; }
            BulletController newBullet = Instantiate(bullet, firePoint.position, firePoint.rotation) as BulletController;
            newBullet.bulletDamage = this.bulletDamage;
            newBullet.speed = bulletSpeed;
            newBullet.enemy = this.enemy;
            Vector3 bulletSpread = (UnityEngine.Random.insideUnitSphere.normalized);
            newBullet.GetComponent<Rigidbody>().velocity =( (bulletSpread * spread   + newBullet.GetComponent<Transform>().forward).normalized * newBullet.speed);
            currentMagazineSize--;
            soundmaker.clip = (AudioClip)Resources.Load("Sounds/GunSounds/pistol/shot" + UnityEngine.Random.Range(1, 3), typeof(AudioClip));
            soundmaker.Play();
        }
    }

    public string getAmmo()
    {
        string s = currentMagazineSize + "/" + magazineSize;
        return s;
    }


	// Use this for initialization
	void Start () {
        reloadSound = (AudioClip)Resources.Load("Sounds/GunSounds/pistol/reload", typeof(AudioClip));
        soundmaker = this.GetComponent<AudioSource>();
        currentMagazineSize = magazineSize;
    }
	
	// Update is called once per frame
	void Update () {

	}

    IEnumerator reloading()
    {
        readyToShoot = false;
        yield return new WaitForSeconds(reloadSpeed);
        currentMagazineSize = magazineSize;
        readyToShoot = true;
    }
}
