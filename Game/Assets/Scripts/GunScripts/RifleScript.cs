﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;
using UnityEditor;

public class RifleScript : MonoBehaviour, IGun {
    EnemyAI enemy;
    public int currentMagazineSize;
    public int magazineSize = 30;
    public BulletController bullet;
    public Transform firePoint;
    public float bulletSpeed = 25.0f;
    public float reloadSpeed = 1.5f;
    private float shootDelay = 0.0f;
    public int bulletDamage = 13;
    public float shotsPerSecond = 3.5f;
    public float spread = 0.2f;
    private bool readyToShoot = true;
    private AudioSource soundmaker;
    private AudioClip reloadClip;
    private AudioClip shotClip;

    public void setHolder(Transform holder) {
        enemy = holder.GetComponent<EnemyAI>();
    }

    public bool gottaReload()
    {
        return currentMagazineSize > 0;
    }

    public void passStatistics(float bulletDamageBoost, float bulletSpeedBoost, float accuracyBoost)
    {
        this.bulletDamage = (int)Math.Round(bulletDamage * (1 + bulletDamageBoost));
        this.bulletSpeed *= (1 + bulletSpeedBoost);
        this.spread *= accuracyBoost;
    }

    public bool isReady()
    {
        return readyToShoot;
    }

    public float getDelay()
    {
        return shootDelay;
    }

    public string getName()
    {
        return "assault_rifle";
    }

    public float getReloadTime()
    {
        return reloadSpeed;
    }

    public void setFirepoint(Transform firepoint)
    {
        this.firePoint = firepoint;
    }

    public void reduceDelay()
    {
        shootDelay -= Time.deltaTime;
    }

    public void reload()
    {
        if (currentMagazineSize < magazineSize)
        {
            soundmaker.clip = reloadClip;
            soundmaker.Play();
            StartCoroutine(reloading());
        }
    }

    public void shoot()
    {
        if (currentMagazineSize > 0 && shootDelay<=0.0f)
        {
            shootDelay = 1.0f / shotsPerSecond;
            BulletController newBullet = Instantiate(bullet, firePoint.position, firePoint.rotation) as BulletController;
            newBullet.bulletDamage = this.bulletDamage;
            newBullet.speed = bulletSpeed;
            newBullet.enemy = this.enemy;
            Vector3 bulletSpread = (UnityEngine.Random.insideUnitSphere.normalized);
            newBullet.GetComponent<Rigidbody>().velocity =( (bulletSpread * spread   + newBullet.GetComponent<Transform>().forward).normalized * newBullet.speed);
            currentMagazineSize--;
            soundmaker.clip = shotClip;
            soundmaker.Play();
        }
    }

    public string getAmmo()
    {
        string s = currentMagazineSize + "/" + magazineSize;
        return s;
    }


	// Use this for initialization
	void Start () {
        soundmaker = this.GetComponent<AudioSource>();
        currentMagazineSize = magazineSize;
        for (int i = 0; i < this.transform.parent.childCount; i++)
        {
            Transform child = this.transform.parent.GetChild(i);
            if (child.name == "Firepoint")
            {
                this.firePoint = child;
            }
        }
        reloadClip = (AudioClip)Resources.Load("Sounds/GunSounds/rifle/reload", typeof(AudioClip));
        shotClip = (AudioClip)Resources.Load("Sounds/GunSounds/rifle/shot1", typeof(AudioClip));
    }
	
	// Update is called once per frame
	void Update () {

	}

    IEnumerator reloading()
    {
        readyToShoot = false;
        yield return new WaitForSeconds(reloadSpeed);
        currentMagazineSize = magazineSize;
        readyToShoot = true;
    }
}
