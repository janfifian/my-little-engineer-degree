﻿using System.Collections;
using System;
using UnityEngine;
using UnityEditor;

public class ShotgunScript : MonoBehaviour, IGun 
{
    EnemyAI enemy;
    public int currentMagazineSize;
    public int magazineSize = 2;
    public BulletController bullet;
    public Transform firePoint;
    public float bulletSpeed = 25.0f;
    public float reloadSpeed = 2.0f;
    private float shootDelay = 0.0f;
    public int bulletDamage = 7;
    public float shotsPerSecond = 7.0f;
    public float spread = 0.4f;
    public float bulletCount = 7;
    private bool readyToShoot = true;
    private AudioSource soundmaker;
    private AudioClip shotSound;
    private AudioClip reloadSound;

    public void setHolder(Transform holder)
    {
        enemy = holder.GetComponent<EnemyAI>();
    }

    public void passStatistics(float bulletDamageBoost, float bulletSpeedBoost, float accuracyBoost)
    {
        this.bulletDamage = (int)Math.Round(bulletDamage * (1 + bulletDamageBoost));
        this.bulletSpeed *= (1 + bulletSpeedBoost);
        this.spread *= accuracyBoost;
    }

    public bool isReady()
    {
        return readyToShoot;
    }

    public bool gottaReload()
    {
        return currentMagazineSize > 0;
    }

    public float getDelay()
    {
        return shootDelay;
    }

    public string getName()
    {
        return "shotgun";
    }

    public void setFirepoint(Transform firepoint)
    {
        this.firePoint = firepoint;
    }

    public void reduceDelay()
    {
        shootDelay -= Time.deltaTime; 
    }

    public void reload()
    {

        soundmaker.clip = reloadSound;
        soundmaker.Play();
        StartCoroutine(reloading());
    }

    public void shoot()
    {
        if (currentMagazineSize > 0 && shootDelay <= 0.0f)
        {
            currentMagazineSize--;
            soundmaker.clip = shotSound;
            soundmaker.Play();
            shootDelay = 1.0f / shotsPerSecond;
            for (int i = 0; i < bulletCount; i++) { 
                BulletController newBullet = Instantiate(bullet, firePoint.position, firePoint.rotation) as BulletController;
                newBullet.bulletDamage = this.bulletDamage;
                newBullet.speed = bulletSpeed;
                newBullet.enemy = this.enemy;
                Vector3 bulletSpread = (UnityEngine.Random.insideUnitSphere.normalized);
                newBullet.GetComponent<Rigidbody>().velocity = ((bulletSpread * spread + newBullet.GetComponent<Transform>().forward).normalized * newBullet.speed);
            }
        }
    }


    // Use this for initialization
    void Start()
    {
        reloadSound = (AudioClip)Resources.Load("Sounds/GunSounds/shotgun/reload", typeof(AudioClip));
        shotSound = (AudioClip)Resources.Load("Sounds/GunSounds/shotgun/shot1", typeof(AudioClip));
#pragma warning disable CS0618 // Type or member is obsolete
        UnityEngine.Random.seed = (int)System.DateTime.Now.Ticks;
#pragma warning restore CS0618 // Type or member is obsolete
        currentMagazineSize = magazineSize;
        soundmaker = this.GetComponent<AudioSource>();
        for (int i = 0; i < this.transform.parent.childCount; i++)
        {
            Transform child = this.transform.parent.GetChild(i);
            if (child.name == "Firepoint")
            {
                this.firePoint = child;
            }
        }
    }

    // Update is called once per frame
    void Update()
    {

    }

    IEnumerator reloading()
    {
        readyToShoot = false;
        yield return new WaitForSeconds(reloadSpeed);
        currentMagazineSize = magazineSize;
        readyToShoot = true;
    }

    public string getAmmo()
    {
        string s = currentMagazineSize + "/" + magazineSize;
        return s;
    }

    public float getReloadTime()
    {
        return reloadSpeed;
    }
}


