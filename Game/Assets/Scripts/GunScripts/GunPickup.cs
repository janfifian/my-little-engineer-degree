﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GunPickup : MonoBehaviour {

    public Transform gun;

    void Start () {
	}
	
	void Update () {	
	}

    private void OnTriggerStay(Collider collision)
    {
        if (collision.gameObject.name == "Player" && Input.GetKeyDown(KeyCode.F))
        {
            Destroy(collision.gameObject.transform.GetChild(collision.gameObject.transform.childCount-1).gameObject);
            Transform newGun = Instantiate(gun, collision.gameObject.transform);
            collision.gameObject.GetComponent<PlayerControls>().gun = newGun.GetComponent<IGun>();
            newGun.GetComponent<IGun>().setFirepoint(collision.transform.GetChild(0).transform);
            collision.gameObject.GetComponent<PlayerControls>().WeaponSwitchAmmoUpdate();
            collision.gameObject.GetComponent<PlayerControls>().AdjustWeapon();
            StartCoroutine(TemporaryDisable());
        }
    }

    IEnumerator TemporaryDisable()
    {
        this.transform.GetChild(0).gameObject.SetActive(false);
        this.GetComponent<BoxCollider>().enabled = (false);
        yield return new WaitForSeconds(20f);
        this.transform.GetChild(0).gameObject.SetActive(true);
        this.GetComponent<BoxCollider>().enabled = (true);
    }
}