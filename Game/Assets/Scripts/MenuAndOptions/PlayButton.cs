﻿using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;
using TMPro;

public class PlayButton : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler, IPointerClickHandler
{
    public void Start()
    {
        if (!PlayerPrefs.HasKey("weapon_choice"))
        {
            PlayerPrefs.SetString("weapon_choice", "Pistol");
        }

        if (!PlayerPrefs.HasKey("skill_choice"))
        {
            PlayerPrefs.SetString("skill_choice","Blink");
        }

        if (!PlayerPrefs.HasKey("bullet_speed_boost"))
        {
            PlayerPrefs.SetInt("bullet_speed_boost", 5);
        }

        if (!PlayerPrefs.HasKey("damage_boost"))
        {
            PlayerPrefs.SetInt("damage_boost", 5);
        }

        if (!PlayerPrefs.HasKey("sight_radius"))
        {
            PlayerPrefs.SetInt("sight_radius", 5);
        }

        if (!PlayerPrefs.HasKey("accuracy_boost"))
        {
            PlayerPrefs.SetInt("accuracy_boost", 5);
        }

        if (!PlayerPrefs.HasKey("health_boost"))
        {
            PlayerPrefs.SetInt("health_boost", 5);
        }

        if (!PlayerPrefs.HasKey("mana_boost"))
        {
            PlayerPrefs.SetInt("mana_boost", 5);
        }

        if (!PlayerPrefs.HasKey("speed_boost"))
        {
            PlayerPrefs.SetInt("speed_boost", 5);
        }
    }

    // When highlighted with mouse.
    public void OnPointerEnter(PointerEventData eventData)
    {
        TextMeshProUGUI tmpgui = this.transform.GetChild(0).GetComponent<TextMeshProUGUI>();
        // Do something.
        tmpgui.color = new Color(255, 10, 0, 255);
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        TextMeshProUGUI tmpgui = this.transform.GetChild(0).GetComponent<TextMeshProUGUI>();
        // Do something.
        tmpgui.color = new Color(255, 255, 255, 255);
    }

    public void SavePrefs()
    {
        StatsUIComponent statuicomp = this.transform.parent.transform.GetComponent<StatsUIComponent>();
        PlayerPrefs.SetInt("bullet_speed_boost", statuicomp.bullet_speed_boost);
        PlayerPrefs.SetInt("damage_boost", statuicomp.damage_boost);
        PlayerPrefs.SetInt("sight_radius", statuicomp.sight_radius);
        PlayerPrefs.SetInt("accuracy_boost", statuicomp.accuracy_boost);
        PlayerPrefs.SetInt("health_boost", statuicomp.health_boost);
        PlayerPrefs.SetInt("mana_boost", statuicomp.mana_boost);
        PlayerPrefs.SetInt("speed_boost", statuicomp.speed_boost);
    }

    public void OnPointerClick(PointerEventData ped)
    {
        if(SceneManager.GetActiveScene().buildIndex == 3)
        {
            SavePrefs();   
        }
        SceneManager.LoadScene(1);
    }
}