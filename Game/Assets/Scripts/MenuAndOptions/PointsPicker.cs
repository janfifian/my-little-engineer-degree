﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEditor;
using TMPro;

public class PointsPicker : MonoBehaviour {

    public StatsUIComponent stats;
    public Image icon;
    public TextMeshProUGUI tmp;
    public int statindex;
    public string statname;
    public int points;
	// Use this for initialization
	void Start () {
        points = stats.GetStat(statindex);
        tmp.text = "" + points;
        icon.overrideSprite = (Sprite)Resources.Load("Sprites/staticons/" + statname + "icon.png", typeof(Sprite));
    }

    public void increment()
    {
        if (points == 10)
        {
            return;
        }
        else
        {
            stats.Increment(statindex);
            points = stats.GetStat(statindex);
            tmp.text = "" + points;
        }
    }

    public void decrement()
    {
        if (points == 0)
        {
            return;
        }
        else
        {
            stats.Decrement(statindex);
            points = stats.GetStat(statindex);
            tmp.text = "" + points;
        }
    }
}
