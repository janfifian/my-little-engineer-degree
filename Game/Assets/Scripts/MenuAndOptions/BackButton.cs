﻿using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;
using TMPro;

public class BackButton : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler, IPointerClickHandler
{
    public TextMeshProUGUI weaponChoice;
    public TextMeshProUGUI skillChoice;

    public void OnPointerEnter(PointerEventData eventData)
    {
        TextMeshProUGUI tmpgui = this.transform.GetChild(0).GetComponent<TextMeshProUGUI>();
        tmpgui.color = new Color(255, 10, 0, 255);
     }

    public void OnPointerExit(PointerEventData eventData)
    {
        TextMeshProUGUI tmpgui = this.transform.GetChild(0).GetComponent<TextMeshProUGUI>();
        tmpgui.color = new Color(255, 255, 255, 255);
    }

    public void saveWeaponAndSkillChoice()
    {
        PlayerPrefs.SetString("weapon_choice", weaponChoice.text);
        PlayerPrefs.SetString("skill_choice", skillChoice.text);
    }

    public void saveStatsChoice()
    {

    }

    public void OnPointerClick(PointerEventData ped)
    {
        if(SceneManager.GetActiveScene().buildIndex == 4)
        {
            saveWeaponAndSkillChoice();
        }
        if(SceneManager.GetActiveScene().buildIndex == 3)
        {
            saveStatsChoice();
            SceneManager.LoadScene(4);
            return;
        }
        
        //In other cases return to main menu.
        SceneManager.LoadScene(0);
    }
}