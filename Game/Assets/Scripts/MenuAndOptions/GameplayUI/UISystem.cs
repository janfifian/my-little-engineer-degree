﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEditor;
using TMPro;

public class UISystem : MonoBehaviour {

    private TextMeshProUGUI ammoText;
    private TextMeshProUGUI lifeText;
    private TextMeshProUGUI manaText;
    private TextMeshProUGUI enemiesText;
    private Image bulletImage;
    public Image skillIcon;
    public Sprite overridingSprite;
    public int score;
    public Texture2D cursor;

	// Use this for initialization
	void Start () {
        bulletImage = this.transform.GetChild(3).transform.GetChild(1).GetComponent<Image>();
        lifeText     = this.transform.GetChild(0).transform.GetChild(0).GetComponent<TextMeshProUGUI>();
        manaText     = this.transform.GetChild(1).transform.GetChild(0).GetComponent<TextMeshProUGUI>();
        enemiesText  = this.transform.GetChild(2).transform.GetChild(0).GetComponent<TextMeshProUGUI>();
        ammoText     = this.transform.GetChild(3).transform.GetChild(0).GetComponent<TextMeshProUGUI>();
        skillIcon    = this.transform.GetChild(4).transform.GetChild(0).GetComponent<Image>();
        Cursor.SetCursor(cursor, Vector2.zero, CursorMode.Auto);
    }
    
    /**Update UI Components:
    * 0 - life
    * 1 - mana
    * 2 - enemies
    * 3 - ammo
    */
    public void UpdateUI(int index, string text)
    {
        switch (index)
        {
            case 0:
                lifeText.text = text;
                break;
            case 1:
                manaText.text = text;
                break;
            case 2:
                enemiesText.text = text;
                break;
            case 3:
                ammoText.text = text;
                break;
        }
    }

    public void updateAmmoIcon(string gunname)
    {
            bulletImage.overrideSprite = (Sprite)Resources.Load("Sprites/BulletSprites/" + gunname +"_bullet", typeof(Sprite));
    }

    public void updateSkillIcon(string skillname)
    {
        Debug.Log("Overriding sprite with " + skillname);
        overridingSprite = (Sprite)Resources.Load("Sprites/" + skillname , typeof(Sprite));
        skillIcon.overrideSprite = overridingSprite;

    }

    public void updateSkillCd(float f)
    {
        skillIcon.fillAmount = f;
    }
}
