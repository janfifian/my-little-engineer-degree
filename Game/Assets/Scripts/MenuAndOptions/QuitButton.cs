﻿using UnityEngine;
using UnityEngine.EventSystems;
using TMPro;

public class QuitButton : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler, IPointerClickHandler
{
    public void OnPointerEnter(PointerEventData eventData)
    {
        TextMeshProUGUI tmpgui = this.transform.GetChild(0).GetComponent<TextMeshProUGUI>();
        tmpgui.color = new Color(255, 10, 0, 255);
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        TextMeshProUGUI tmpgui = this.transform.GetChild(0).GetComponent<TextMeshProUGUI>();
        tmpgui.color = new Color(255, 255, 255, 255);
    }

    public void OnPointerClick(PointerEventData ped)
    {
        Application.Quit();
    }
}