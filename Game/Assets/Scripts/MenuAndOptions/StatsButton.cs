﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;
using UnityEngine.Experimental.UIElements;
using UnityEngine.UI;

public class StatsButton :  MonoBehaviour, IPointerEnterHandler, IPointerExitHandler, IPointerClickHandler
{
    public TextMeshProUGUI weaponChoice;
    public TextMeshProUGUI skillChoice;
    public UnityEngine.UI.Toggle ezModeToggle;
    public Dropdown ddValue;

    public void OnPointerEnter(PointerEventData eventData)
    {
        TextMeshProUGUI tmpgui = this.transform.GetChild(0).GetComponent<TextMeshProUGUI>();
        tmpgui.color = new Color(255, 10, 0, 255);
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        TextMeshProUGUI tmpgui = this.transform.GetChild(0).GetComponent<TextMeshProUGUI>();
        tmpgui.color = new Color(255, 255, 255, 255);
    }

    public void OnPointerClick(PointerEventData ped)
    {
        //Stats Scene has index 3
        PlayerPrefs.SetString("weapon_choice", weaponChoice.text);
        PlayerPrefs.SetString("skill_choice", skillChoice.text);
        PlayerPrefs.SetInt("ez_mode", ezModeToggle.isOn ? 1 : 0);
        Debug.Log("SavedAsEzMode: " +( ezModeToggle.isOn ? 1 : 0).ToString());
        PlayerPrefs.SetInt("enemies_generation_mode", ddValue.value);
        SceneManager.LoadScene(3);
    }
}
