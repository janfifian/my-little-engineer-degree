﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class StatsUIComponent : MonoBehaviour {

    public int bullet_speed_boost = 5;
    public int damage_boost = 5;
    public int sight_radius = 5;
    public int accuracy_boost = 5;
    public int health_boost = 5;
    public int mana_boost = 5;
    public int speed_boost = 5;
    public TextMeshProUGUI tmpro;
    // Use this for initialization

    private void Start()
    {
        UpdateText();
    }
    public int getTotal () {
        return bullet_speed_boost + damage_boost + sight_radius + accuracy_boost
            + health_boost + mana_boost + speed_boost;
    }
	
	// Update is called once per frame
	void UpdateText() {
        tmpro.text = "Points left: <color=\"red\">" + (35-getTotal());
    }

    public void Increment(int index)
    {
        if(getTotal() == 35)
        {
            //no points above max!
            return;
        }
        switch (index)
        {
            case 1:
                bullet_speed_boost++;
                break;
            case 2:
                damage_boost++;
                break;
            case 3:
                sight_radius++;
                break;
            case 4:
                accuracy_boost++;
                break;
            case 5:
                health_boost++;
                break;
            case 6:
                mana_boost++;
                break;
            case 7:
                speed_boost++;
                break;
        }
        UpdateText();
    }

    public void Decrement(int index)
    {
        switch (index)
        {
            case 1:
                if(bullet_speed_boost>0)
                bullet_speed_boost--;
                break;
            case 2:
                if(damage_boost>0)
                damage_boost--;
                break;
            case 3:
                if(sight_radius>0)
                sight_radius--;
                break;
            case 4:
                if(accuracy_boost>0)
                accuracy_boost--;
                break;
            case 5:
                if(health_boost>0)
                health_boost--;
                break;
            case 6:
                if(mana_boost>0)
                mana_boost--;
                break;
            case 7:
                if(speed_boost>0)
                speed_boost--;
                break;
        }
        UpdateText();
    }

    public int GetStat(int index)
    {
        switch (index)
        {
            case 1:
                return bullet_speed_boost;

            case 2:
                return damage_boost;
                
            case 3:
                return sight_radius;
                
            case 4:
                return accuracy_boost;
                
            case 5:
                return health_boost;
                
            case 6:
                return mana_boost;
                
            case 7:
                return speed_boost;
                
        }
        return -1;
    }
}
