﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class SkillPicker : MonoBehaviour {

    private TextMeshProUGUI tmpro;
    private Slider mainSlider;

    void Start()
    {
        tmpro = this.transform.GetChild(3).GetComponent<TextMeshProUGUI>();
        mainSlider = this.GetComponent<Slider>();
        mainSlider.onValueChanged.AddListener(delegate { ValueChanged(); });
    }
    void ValueChanged()
    {
        switch ((int)mainSlider.value)
        {
            case 0:
                tmpro.text = "Blink";
                break;
            case 1:
                tmpro.text = "Pacify";
                break;
            case 2:
                tmpro.text = "Turret";
                break;
            case 3:
                tmpro.text = "Table Drop";
                break;
            case 4:
                tmpro.text = "Knife Throw";
                break;
            case 5:
                tmpro.text = "Sprint";
                break;
            case 6:
                tmpro.text = "Mend Wounds";
                break;
            case 7:
                tmpro.text = "Light Regen";
                break;
        }


    }
}
