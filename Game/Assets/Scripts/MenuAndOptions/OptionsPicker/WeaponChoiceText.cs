﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class WeaponChoiceText : MonoBehaviour {

    private TextMeshProUGUI tmpro;
    private Slider mainSlider;

    void Start () {
        tmpro = this.transform.GetChild(3).GetComponent<TextMeshProUGUI>();
        mainSlider = this.GetComponent<Slider>();
        mainSlider.onValueChanged.AddListener(delegate { ValueChanged(); });
    }
    void ValueChanged()
    {
        switch ((int)mainSlider.value){
            case 0:
                tmpro.text = "Pistol";
                break;
            case 1:
                tmpro.text = "Shotgun";
                break;
            case 2:
                tmpro.text = "Sniper Rifle";
                break;
            case 3:
                tmpro.text = "Assault Rifle";
                break;
            case 4:
                tmpro.text = "UZI";
                break;
        }


    }

}
